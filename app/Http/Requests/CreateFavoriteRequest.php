<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CreateFavoriteRequest extends FormRequest
{
    use PermissionCheckTrait;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['favorite create']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'productId' => 'required|exists:products,id'
        ];
    }
}
