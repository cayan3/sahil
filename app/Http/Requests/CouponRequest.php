<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class CouponRequest extends FormRequest
{
    use PermissionCheckTrait;
    
    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['coupon update', 'coupon create']);
    }

    public function rules(): array
    {
        return [
            'expire' => ['required', 'date', 'after:' . Carbon::today()],
            'discount' => 'required|numeric|min:0|max:100',
            "status" => 'required'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $redirectUrl = $this->isMethod('post') 
            ? route('coupons.create') 
            : route('coupons.edit', ['coupon' => $this->route('coupon')]);
        
        throw new HttpResponseException(
            redirect($redirectUrl)
                ->withErrors($validator)
                ->withInput()
        );
    }
}
