<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class ReviewRequest extends FormRequest
{
    use PermissionCheckTrait;

    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['review create', 'review update']);
    }

    public function rules(): array
    {
        return [
            "rating" => 'required|integer|min:1|max:5',
            'comment' => 'nullable|string|max:255',
            'productId' => 'required|numeric|exists:products,id'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
