<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class SubCategoryRequest extends FormRequest
{
    use PermissionCheckTrait;
    
    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['sub_category create', 'sub_category update']);
    }

    public function rules(): array
    {
        if ($this->isMethod('post')) {
            return [
            'title' => 'required|string',
            'status' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:10240',
            'category_id' => 'required',
            ];
        }else{
            return [
            'title' => 'required|string',
            'status' => 'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:10240',
            'category_id' => 'required',
            ];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        $redirectUrl = $this->isMethod('post') 
            ? route('subcategories.create') 
            : route('subcategories.edit', ['subcategory' => $this->route('subcategory')]);
        
        throw new HttpResponseException(
            redirect($redirectUrl)
                ->withErrors($validator)
                ->withInput()
        );
    }
}
