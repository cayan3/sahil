<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class ProductRequest extends FormRequest
{
    use PermissionCheckTrait;
    
    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['product create']);
    }

    public function rules(): array
    {
        return [
            'description' => 'required',
            'title' => 'required|string',
            'status' => 'required|numeric',
            'imageCover' => 'required|image|mimes:jpeg,png,jpg,gif|max:10240',
            'price' => 'required',
            'color_id' => 'required',
            'size_id' => 'required',
            'quantity' => 'required',

            'category_id' => 'required|exists:categories,id',
            'sub_category_id' => 'required|exists:sub_categories,id',
            'brand_id' => 'required|exists:brands,id',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
