<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class AddressRequest extends FormRequest
{
    use PermissionCheckTrait;

    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['address Update', 'address create']);
    }

    public function rules(): array
    {

        $method = $this->method();

        if ($method == 'POST') {

            return [
                'governorateId' => 'required|numeric|exists:governorates,id',
                'cityId' => 'required|numeric|exists:cities,id',
                'district' => 'required|string|max:255',
                'default' => 'nullable|boolean',
            ];
        } elseif ($method == 'PUT' || $method == 'PATCH') {

            return [
                'governorateId' => 'sometimes|numeric|exists:governorates,id',
                'cityId' => 'sometimes|numeric|exists:cities,id',
                'district' => 'sometimes|string|max:255',
                'default' => 'sometimes|boolean',
            ];
        }

        return [];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
