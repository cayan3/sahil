<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class ReportRequest extends FormRequest
{
    use PermissionCheckTrait;
  
    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['report create', 'report update']);
    }

    public function rules(): array
    {
        return [
            'productId' => 'required|numeric|exists:products,id',
            'reportData' => 'required|string|max:255',
        ];
    }
    
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
