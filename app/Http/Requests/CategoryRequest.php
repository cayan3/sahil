<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class CategoryRequest extends FormRequest
{
    use PermissionCheckTrait;
    
    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['category create', 'category update']);
    }

    public function rules(): array
    {
        $categoryId = $this->route('category');

        if ($this->isMethod('post')) {
            return [
                'title' => 'required|string|unique:categories,title,' . $categoryId,
                'status' => 'required|integer',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:10240',
                'sizes' => 'required|array',
                'sizes.*' => 'required|string',
            ];
        }else{
            return [
                'title' => 'nullable|string|unique:categories,title,' . $categoryId,
                'status' => 'nullable|integer',
                'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:10240',
                'sizes' => 'nullable|array',
                'sizes.*' => 'required|string',
            ];
        }
        
    }

    protected function failedValidation(Validator $validator)
    {
        $redirectUrl = $this->isMethod('post') 
            ? route('categories.create') 
            : route('categories.edit', ['category' => $this->route('category')]);
        
        throw new HttpResponseException(
            redirect($redirectUrl)
                ->withErrors($validator)
                ->withInput()
        );
    }
}
