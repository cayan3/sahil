<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class BrandRequest extends FormRequest
{
    use PermissionCheckTrait;
  
    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['brand create', 'brand update']);
    }

    public function rules(): array
    {
        $brandId = $this->route('brand');

        return [
            'title' => 'required|string|unique:brands,title' . $brandId,
            'status' => 'required|integer',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:10240',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $redirectUrl = $this->isMethod('post') 
            ? route('brands.create') 
            : route('brands.edit', ['brand' => $this->route('brand')]);
        
        throw new HttpResponseException(
            redirect($redirectUrl)
                ->withErrors($validator)
                ->withInput()
        );
    }
}
