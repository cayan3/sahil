<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class EmployeeRequest extends FormRequest
{
    use PermissionCheckTrait;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['employee create', 'employee update']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:50',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|string|size:11',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $redirectUrl = $this->isMethod('post') 
            ? route('employees.create') 
            : route('employees.edit', ['employee' => $this->route('employee')]);
        
        throw new HttpResponseException(
            redirect($redirectUrl)
                ->withErrors($validator)
                ->withInput()
        );

    }
}
