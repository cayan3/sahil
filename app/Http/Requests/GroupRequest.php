<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class GroupRequest extends FormRequest
{
    use PermissionCheckTrait;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['group create', 'group update']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'agent_id' => 'required|integer|exists:users,id',
            'orders' => 'required|array',
            'orders.*' => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'numbers.required' => 'The numbers field is required.',
            'numbers.array' => 'The numbers field must be an array.',
            'numbers.*.required' => 'Each number in the array is required.',
            'numbers.*.integer' => 'Each number must be an integer.',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
