<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class SettingRequest extends FormRequest
{
    use PermissionCheckTrait;

    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['setting update']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'site_name' => 'required|max:50',
            'site_email' => 'required|email',
            'site_logo' => 'nullable|image',
            'facebook_url' => 'nullable|url',
            'twitter_url' => 'nullable|url',
            'instagram_url' => 'nullable|url',
            'commission' => 'required|numeric',
            'commission_status' => 'required|in:percentage,standard',
            'shipping_fees' => 'required|numeric',
            'shipping_fees_status' => 'required|in:percentage,standard'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $redirectUrl = route('settings.index');
        
        throw new HttpResponseException(
            redirect($redirectUrl)
                ->withErrors($validator)
                ->withInput()
        );
    }
}
