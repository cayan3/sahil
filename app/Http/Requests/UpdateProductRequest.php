<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class UpdateProductRequest extends FormRequest
{
    use PermissionCheckTrait;
    
    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['product update']);
    }

    public function rules(): array
    {
        return [
            'description' => 'nullable',
            'title' => 'nullable|string',
            'status' => 'nullable|numeric',
            'imageCover' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:10240',
            'price' => 'nullable|min:0',
            'color' => 'nullable',
            'size' => 'nullable',
            'qty' => 'nullable',

            'category' => 'required|exists:categories,id',
            'subCategory' => 'required|exists:sub_categories,id',
            'brand' => 'required|exists:brands,id',
            'productDetailId' => 'nullable|exists:product_details,id',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
