<?php

namespace App\Http\Requests;

use App\traits\PermissionCheckTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class ReturnRequest extends FormRequest
{
    use PermissionCheckTrait;
    
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(Request $request): bool
    {
        return $this->PermissionCheck($request, ['return create']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'returnReason' => 'required|string',
            'orderId' => 'required|numeric|exists:orders,id',
            'returnProducts' => 'required|array',
            'returnProducts.*.orderProductId' => 'required|numeric|exists:order_products,id',
            'returnProducts.*.orderProductQuantity' => 'required|numeric'
        ];
    }
    
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
