<?php

namespace App\Http\Controllers\sahil_agents;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderStatusRequest;
use App\Models\Order;
use App\Services\AgentOrderService;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;

    public function getCurrentOrders(Request $request, AgentOrderService $agentOrderService){
        $this->PermissionCheck($request, ['order view']);
        return $agentOrderService->getCurrentOrders($request);
    }

    public function getPreviousOrders(Request $request, AgentOrderService $agentOrderService){
        $this->PermissionCheck($request, ['order view']);
        return $agentOrderService->getPreviousOrders($request);
    }

    public function getCanceledOrders(Request $request, AgentOrderService $agentOrderService){
        $this->PermissionCheck($request, ['order view']);
        return $agentOrderService->getCanceledOrders($request);
    }

    public function setOrderComplete($id, Request $request, AgentOrderService $agentOrderService) {
        $this->PermissionCheck($request, ['order update']);
        return $agentOrderService->setOrderComplete($id, $request);
    }

    public function setOrderCancel($id, Request $request, AgentOrderService $agentOrderService) {
        $this->PermissionCheck($request, ['order update']);
        return $agentOrderService->setOrderCancel($id, $request);
    }

    public function show($id, Request $request, AgentOrderService $agentOrderService) {
        $this->PermissionCheck($request, ['order view']);
        return $agentOrderService->show($id);
    }

    public function statusOrder(OrderStatusRequest $request) {
        $order = Order::find($request->orderId);
        if ($order->is_paid == 1 && $request->orderStatus == 'canceled') {
            return $this->errorResponse('You cannot cancel a paid order', 500);
        }

        if ($order->order_status == 'completed') {
            return $this->errorResponse('This Order is completed', 500);
        }

        $order->order_status = $request->orderStatus;
        $order->save();
    
        return $this->successResponse('Order Status Updated');
    }
}
