<?php

namespace App\Http\Controllers\sahil_agents;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentRequest;
use App\Services\PaymentService;
use App\traits\PermissionCheckTrait;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    use PermissionCheckTrait;

    public function index(Request $request, PaymentService $paymentService){
        $this->PermissionCheck($request, ['payment view']);
        return $paymentService->index();
    }
    
    public function store(PaymentRequest $request, PaymentService $paymentService){
        return $paymentService->store($request);
    }
    
    public function show(string $id, Request $request, PaymentService $paymentService){
        $this->PermissionCheck($request, ['payment view']);
        return $paymentService->show($id);
    }
    
    public function update(PaymentRequest $request, string $id, PaymentService $paymentService){
        return $paymentService->update($request, $id);
    }
    
    public function destroy(string $id, Request $request, PaymentService $paymentService){
        $this->PermissionCheck($request, ['payment delete']);
        return $paymentService->destroy($id);
    }

    public function agentPayments(string $id, Request $request, PaymentService $paymentService)  {
        $this->PermissionCheck($request, ['payment view']);
        return $paymentService->agentPayments($id);
    }
}
