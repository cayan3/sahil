<?php

namespace App\Http\Controllers\sahil_agents;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Services\ProfileService;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    use ResponseTrait;

    public function me(Request $request, ProfileService $authService) {
        return $authService -> me($request);
    }
    
    public function changePassword(ChangePasswordRequest $request, ProfileService $authService){
        return $authService -> changePassword($request);
    }

    public function verifyEmail (Request $request, ProfileService $authService){
        return $authService -> verifyEmail($request);
    }
}
