<?php

namespace App\Http\Controllers\Sahil_Agents;

use App\Http\Controllers\Controller;
use App\Http\Requests\GroupRequest;
use App\Http\Resources\Resource;
use App\Services\GroupService;
use App\traits\HelperTrait;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    use PermissionCheckTrait, HelperTrait, ResponseTrait;

    public function commission(Request $request, GroupService $groupService) {
        return $groupService->commission($request);
    }

    public function totalAmount(Request $request, GroupService $groupService) {
        return $groupService->totalAmount($request);
    }

    public function index(Request $request, GroupService $groupService){
        $this->PermissionCheck($request, ['group view']);
        $groups = $groupService->index($request);

        $data = Resource::collection($groups);
        return $this->dataResponse('All of Groups', $data);
    }

    public function store(GroupRequest $request, GroupService $groupService){
        return $groupService->store($request);
    }

    public function show(string $id, Request $request, GroupService $groupService){
        return $groupService->show($id, $request);
    }

    public function update(GroupRequest $request, string $id, GroupService $groupService){
        return $groupService->update($id,$request);
    }

    public function destroy(string $id, Request $request, GroupService $groupService){
        $this->PermissionCheck($request, ['group delete']);
        return $groupService->destroy($id);
    }

    public function updateGroupStatus(string $id, Request $request, GroupService $groupService){
        $this->PermissionCheck($request, ['group update']);
        return $groupService->updateGroupStatus($id, $request);
    }
}
