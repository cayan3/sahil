<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\GetCitiesRequest;
use App\Http\Resources\Resource;
use App\Models\City;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class CityController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;
    
    public function index(GetCitiesRequest $request)
    {
        $cities = City::where('governorate_id', $request->governorateId)->get();
        $data = Resource::collection($cities);
        return $this->dataResponse(' All Cities',$data );
    }
}
