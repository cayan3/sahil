<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Resource;
use App\Models\Governorate;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class GovernorateController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;

    public function index()
    {
        $governorates = Governorate::all();
        $data = Resource::collection($governorates);
        return $this->dataResponse(' All Governorates',$data );
    }
}
