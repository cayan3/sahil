<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailLoginRequest;
use App\Http\Requests\PhoneLoginRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\VendorRegisterRequest;
use App\Services\AuthService;
use App\Services\UserService;
use App\Services\VendorService;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use ResponseTrait;

    public function userRegister(AuthService $authService, UserRequest $request, UserService $userService) {
        return $authService -> userRegister($userService, $request);
    }

    public function vendorRegister(AuthService $authService, VendorService $vendorService, VendorRegisterRequest $request, UserService $userService){
        return $authService -> vendorRegister($userService, $request, $vendorService);
    }

    public function loginWithEmail(EmailLoginRequest $request, AuthService $authService) {
        return $authService -> loginWithEmail($request);
    }

    public function loginWithPhone(PhoneLoginRequest $request, AuthService $authService) {
        return $authService -> loginWithPhone($request);
    }

    public function logout(Request $request, AuthService $authService) {
        return $authService -> logout($request);
    }

    public function resetPassword(ResetPasswordRequest $request, AuthService $authService, UserService $userService) {
        return $authService -> resetPassword($request, $userService);
    }
}
