<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewRequest;
use App\Services\ReviewService;
use App\traits\PermissionCheckTrait;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    use PermissionCheckTrait;

    public function store(ReviewRequest $request, ReviewService $reviewService){
        return $reviewService -> store($request);
    }

    public function show(Request $request, string $id, ReviewService $reviewService){
        $this->PermissionCheck($request, ['review view']);
        return $reviewService -> show($id);
    }

    public function update(ReviewRequest $request, string $id, ReviewService $reviewService){
        return $reviewService -> update($request, $id);
    }

    public function destroy(Request $request, string $id, ReviewService $reviewService){
        $this->PermissionCheck($request, ['review delete']);
        return $reviewService -> destroy($id);
    }
}
