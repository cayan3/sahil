<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Models\User;
use App\Models\Verification_code;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OtpController extends Controller
{
    use ResponseTrait;

    public function sendOtpByPhone(Request $request) {
        $request->validate([
            'phone' => 'required|exists:users,phone|size:11',
            'use_case' => 'required|string',
        ]);
        
        $user = User::where('phone', $request->phone)->first();
    
        $code = rand(100000, 999999);
        $this->createVerificationCode($request, $user->id, $code);

        $this->sendByPhone($code);

       return $this->successResponse('Phone sent successfully');
    }

    public function sendOtpByEmail(Request $request) {
        $request->validate([
            'email' => 'required|email|exist:users,email',
            'use_case' => 'required|string',
        ]);
        
        $user = User::where('email', $request->email)->first();
        
        $code = rand(100000, 999999);
        $this->createVerificationCode($request, $user->id, $code);

        Mail::to($request->email)->send(new SendMail($code, $request->use_case, 'Use this code to verify email'));

       return $this->successResponse('Email sent successfully');
    }

    public function verifyOtp(Request $request) {

        $request->validate([
            'code' => 'required',
            'use_case' => 'required',
        ]);

        $verificationCode = Verification_code::where('code' ,$request->code)->first();

        if(
            $verificationCode && 
            now()->lessThanOrEqualTo($verificationCode->expire) && 
            $verificationCode->use_case == $request -> use_case
            ){

            $verificationCode->delete();
            return $this->successResponse('Code Verified');
        }
        return $this->errorResponse('Invalid Code', 403);
    }

    private function createVerificationCode($request, $userId, $code) {
        Verification_code::create([
            'code' => $code,
            'expire' => now()->addMinutes(10),
            'use_case' => $request->use_case,
            'user_id' => $userId,
        ]);
    }

    private function sendByPhone($code) {
        $basic  = new \Vonage\Client\Credentials\Basic("78993aa7", "SIqZimON7L2fObFC");
        $client = new \Vonage\Client($basic);

        $client->sms()->send(
            new \Vonage\SMS\Message\SMS("201022904593", 'SAHIL', 'Your Verification Code is ' . $code)
        );$basic  = new \Vonage\Client\Credentials\Basic("78993aa7", "SIqZimON7L2fObFC");
        $client = new \Vonage\Client($basic);

        $client->sms()->send(
            new \Vonage\SMS\Message\SMS("201022904593", 'SAHIL', 'Your Verification Code is ' . $code)
        );
    }
}
