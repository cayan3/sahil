<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateFavoriteRequest;
use App\Services\FavoriteService;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    public function index(Request $request, FavoriteService $favoriteService){
        return $favoriteService->index($request);
    }

    public function store(CreateFavoriteRequest $request, FavoriteService $favoriteService){
        return $favoriteService->store($request);
    }

    public function destroy(string $id, Request $request, FavoriteService $favoriteService){
        return $favoriteService->destroy($request, $id);
    }
}
