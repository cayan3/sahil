<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddressRequest;
use App\Http\Resources\Resource;
use App\Models\Address;
use App\Models\City;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddressController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;

    public function index(Request $request){
        $this->PermissionCheck($request, ['address view']);
        $addresses = Address::where('user_id', Auth::user()->id)->with(['city','governorate'])->get();
        $data = Resource::collection($addresses);
        return $this->dataResponse(' All Addresses',$data );
    }

    public function store(AddressRequest $request){
        $this->PermissionCheck($request, ['address create']);
        
        if (!$this->isCityBelongeToGovernorate($request)) {
            return $this -> errorResponse('This city not belonge to this governorate', 500);
        }

        $addressData = $request->validated();
        $addressData['city_id'] = $request->cityId;
        $addressData['governorate_id'] = $request->governorateId;
        $addressData['user_id'] = Auth::id();
        $address = Address::create($addressData);
        
        $data = new Resource($address);
        $message = 'Address Created';
        return $this -> dataResponse($message, $data);
    }

    public function show(string $id, Request $request){
        $this->PermissionCheck($request, ['address view']);
        
        $address = Address::find($id);
        if (!$address) {
            return $this -> errorResponse('Address Not Found', 404);
        }
        $data = new Resource($address);
        return $this -> dataResponse('Success', $data);
    }

    public function update(AddressRequest $request, string $id){
        $this->PermissionCheck($request, ['address update']);
        
        $address = $this->isAddressExist($id);
        if (!$address) {
            return $this -> errorResponse('Address Not Found', 404);
        }

        $addressData = $request->validated();
        $addressData['city_id'] = $request->cityId;
        $addressData['governorate_id'] = $request->governorateId;
        $address->update($addressData);

        $data = new Resource($address);
        return $this -> dataResponse('Address Updated', $data);
    }

    public function destroy(Request $request, string $id){
        $this->PermissionCheck($request, ['address delete']);
        if (!$this->isAddressExist($id)) {
            return $this -> errorResponse('Address Not Found', 404);
        }        
        return $this -> successResponse('Address Deleted');
    }

    public function isCityBelongeToGovernorate($request) {
        $city = City::where('id', $request -> cityId)
                    ->where('governorate_id', $request -> governorateId)->first();
        if (!$city) {
            return false;
        }
        return true;
    }

    public function isAddressExist($id){
        $address = Address::where('id', $id)->where('user_id', auth()->id())->first();
        if (!$address) {
            return false;
        }
        return $address;
    }
}