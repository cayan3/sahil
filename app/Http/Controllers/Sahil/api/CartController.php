<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CartRequest;
use App\Http\Requests\UpdateCartRequest;
use App\Services\CartService;
use App\traits\PermissionCheckTrait;
use Illuminate\Http\Request;

class CartController extends Controller
{
    use PermissionCheckTrait;

    public function store(CartRequest $request, CartService $cartService){
        $this->PermissionCheck($request, ['cart add']);
        return $cartService->store($request);
    }

    public function show(Request $request, string $id, CartService $cartService){
        $this->PermissionCheck($request, ['cart view']);
        
        return $cartService -> getOneCart($id);
    }

    public function update(UpdateCartRequest $request, string $id, CartService $cartService){
        $this->PermissionCheck($request, ['cart update']);
        return $cartService -> update($request, $id);
    }

    public function destroy(Request $request, string $id, CartService $cartService){
        $this->PermissionCheck($request, ['cart delete']);
        return $cartService -> destroy($id);
    }
}
