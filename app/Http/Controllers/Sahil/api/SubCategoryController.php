<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Resource;
use App\Models\SubCategory;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;

    public function index(Request $request){
        $this->PermissionCheck($request, ['sub_category view']);
        $sub_categories = SubCategory::latest()->paginate(15);

        $data = Resource::collection($sub_categories)->response()->getData(true);
        return $this->dataResponse('Success', $data);
    }

    public function show(Request $request, string $id){
        $this->PermissionCheck($request, ['sub_category view']);
        $sub_category = SubCategory::find($id);
        if (!$sub_category) {
            return $this->errorResponse('No Sub Category For This ID: ' . $id, 404);
        }

        $data = new Resource($sub_category);
        return $this->dataResponse('Sub Category', $data);
    }
}
