<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReturnRequest;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Order_Products;
use App\Models\Product;
use App\Models\Return_Products;
use App\Models\Returns;
use App\traits\HelperTrait;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\MockObject\ReturnValueNotConfiguredException;
use Stripe\Refund;
use Stripe\Stripe;

class ReturnController extends Controller
{
    use HelperTrait, ResponseTrait, PermissionCheckTrait;

    public function store(ReturnRequest $request) {
        $order = Order::find($request->orderId);
        
        if($order->is_paid == 0 || $order->order_status != 'completed'){
            return $this->errorResponse("Can't Return This Order", 406);
        }

        $orderProducts = $this->getOrderProducts($request);

        // update product quantity and sold
        $this->updateProductQuantityAndSold($orderProducts);

        $return = $this->createReturn($request);

        // store data in return products and return price
        $returnPirce = $this->storeReturnProducts($request, $orderProducts, $return);

        $this->refundIfPaymentMethodIsCard($order, $returnPirce);
        
        return $this->successResponse('Order has been returned successfully');
    }

    private function updateProductQuantityAndSold($orderProducts) {
        $updateData = [];
        $quantityStatement = '';
        $soldStatement = '';

        foreach ($orderProducts as $orderProduct) {
            $quantity = $orderProduct->quantity;
            $id = $orderProduct->product_id;
            $updateData[] = [
                'id' => $id,
                'quantity' => $quantity,
            ];
            $quantityStatement .= "WHEN {$id} THEN quantity + {$quantity} ";
            $soldStatement .= "WHEN {$id} THEN sold - {$quantity} ";
        }

        $productIds = array_column($updateData, 'id');

        $products = Product::whereIn('id', $productIds)
            ->update([
                'sold' => DB::raw("CASE id $soldStatement END"),
                'quantity' => DB::raw("CASE id $quantityStatement END"),
            ]);

        return $products;
    }

    private function getOrderProducts($request) {
        $orderProductIds = [];
        foreach($request->returnProducts as $returnProduct){
            $orderProductIds[] = $returnProduct['orderProductId'];
        }
        
        $orderProducts = Order_Products::where('order_id', $request->orderId)
                                    ->whereIn('id', $orderProductIds)
                                    ->get();
        return $orderProducts;
    }

    private function createReturn($request) {
        $code = $this->CreateRandomCode();
        $return = Returns::create([
            'user_id' => auth()->id(),
            'order_id' => $request -> orderId,
            'return_reason' => $request -> returnReason,
            'code' => $code 
        ]);

        return $return;
    }

    private function storeReturnProducts($request, $orderProducts, $return) {
        $returnProducts = $request->returnProducts;
        $returnData = [];
        $returnPirce = 0;
        for ($i=0; $i < count($returnProducts) ; $i++) { 
            for ($x=0; $x < count($orderProducts) ; $x++) { 
                if($orderProducts[$i]->product_id = $returnProducts[$x]['orderProductId']){

                    if (($orderProducts[$i]->quantity < $returnProducts[$x]['orderProductQuantity'])) {
                        return $this->errorResponse('Not Allawed For This Quantity', 406);
                    }

                    $returnData[]=[
                        'order_product_id' => $returnProducts[$x]['orderProductId'],
                        'return_id' => $return->id,
                        'quantity' => $returnProducts[$x]['orderProductQuantity'],
                    ];
                    $returnPirce += $orderProducts[$i]->unit_price * $returnProducts[$x]['orderProductQuantity'];
                }
            }
        }
        Return_Products::insert($returnData);

        return $returnPirce;
    }

    private function refundIfPaymentMethodIsCard($order, $returnPirce) {
        if ($order->payment_method == 'card') {
            // refound money to user
            $refund_price = $returnPirce;
            if($order->coupon_id){
                $coupon = Coupon::find($order->coupon_id);

                $discount_amount = $returnPirce * $coupon->discount / 100;
                $refund_price = $refund_price - $discount_amount;

                $refund_price = $refund_price - $order->shipping_fees;
            }

            Stripe::setApiKey(env('STRIPE_SECRET'));

            try {
                Refund::create([
                    'charge' => $order->stripe_charge_id,
                    'amount' => $refund_price * 100,
                ]);

                return $this->successResponse('Order has been returned and refunded successfully');
            } catch (\Exception $e) {
                return $this->errorResponse('Refund failed: ' . $e->getMessage(), 500);
            }
        }
    }
}