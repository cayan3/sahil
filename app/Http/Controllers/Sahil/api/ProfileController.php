<?php

namespace App\Http\Controllers\sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\VerifyEmailRequest;
use App\Services\ProfileService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function me(Request $request, ProfileService $profileService) {
        return $profileService -> me($request);
    }
    
    public function changePassword(ChangePasswordRequest $request, ProfileService $profileService){
        return $profileService -> changePassword($request);
    }

    public function verifyEmail (VerifyEmailRequest $request, ProfileService $profileService){
        return $profileService -> verifyEmail($request);
    }
}
