<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Resource;
use App\Models\Brand;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;

    public function index(Request $request){
        $this->PermissionCheck($request, ['brand view']);
        $brands = Brand::paginate(1);
        $data = Resource::collection($brands)->response()->getData(true);
        return $this->dataResponse('All Brands', $data);
    }

    public function show(Request $request, string $id){
        $this->PermissionCheck($request, ['brand view']);
        $brand = Brand::find($id);
        if (!$brand) {
            return $this->errorResponse( 'No Brand For This ID: ' . $id, 404);
        }
        $data = new Resource($brand);
        return $this->dataResponse('brand', $data);
    }
}
