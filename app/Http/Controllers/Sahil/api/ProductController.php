<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Resource;
use App\Models\Product;
use App\Services\ProductService;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use ResponseTrait;

    public function index(Request $request, ProductService $productService){
        $data = $productService -> index($request);

        $responseData = Resource::collection($data->products)->response()->getData(true);
        return $this->dataResponse('Success', $responseData);
    }

    public function show(string $id){
        $product = Product::with('productDetails')->find($id);
        if (!$product) {
            return $this->errorResponse('Product Not Found', 404);
        }
        $data = new Resource($product);
        return $this->dataResponse("Order", $data);
    }

    public function getReportsForSpecificProduct($id) {
        $product = Product::find($id);
        if (!$product) {
            return $this->errorResponse('No Product For this ID: ' . $id, 404);
        }
        $data = Resource::collection($product->reports);
        return $this -> dataResponse('Product Reports', $data);

    }
}
