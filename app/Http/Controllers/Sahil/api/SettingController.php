<?php

namespace App\Http\Controllers\sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Resource;
use App\Models\Setting;
use App\traits\ResponseTrait;

class SettingController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $settings = Setting::first();

        if ($settings) {
            $settings->getFirstMedia('site_logo');
        }

        $data = new Resource($settings);

        return $this->dataResponse('Settings', $data);
    }
}
