<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\traits\ResponseTrait;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    use ResponseTrait;
    
    public function redirectToGoogle() {
        return Socialite::driver('google')->redirect();
    }

    public function googleCallback() {
        try {
            $user = Socialite::driver('google')->stateless()->user();
            
            $existUser = User::where('email', $user -> getEmail())->first();
            if($existUser){
                $token = $existUser->createToken('auth_token')->plainTextToken;

                $data = ['accessToken' => $token];
                return $this->dataResponse('Login successful', $data);
            }

            $newUser = User::create([
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'password' => Hash::make('my-google'),
                'provider_id' => $user->id,
                'provider' => 'google',
            ]);

            $token = $newUser->createToken('auth_token')->plainTextToken;
            $data = ['accessToken' => $token];
            return $this->dataResponse('Login successful', $data);
        } catch (\Exception $e) {
            return $this->errorResponse('Authentication failed: ' . $e->getMessage(), 500);
        }
    }
}
