<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReportRequest;
use App\Services\ReportService;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;

    public function index(Request $request, ReportService $reportService){
        $this->PermissionCheck($request, ['report view']);
        return $reportService->getAllReportsForSpecificUser($request);
    }

    public function store(ReportRequest $request, ReportService $reportService){
        $this->PermissionCheck($request, ['report create']);
        return $reportService->store($request);
    }

    public function show(Request $request, string $id, ReportService $reportService){
        $this->PermissionCheck($request, ['report view']);
        return $reportService->show($id);
    }

    public function update(ReportRequest $request, string $id, ReportService $reportService){
        $this->PermissionCheck($request, ['report update']);
        return $reportService->update($id, $request);
    }

    public function destroy(Request $request, string $id, ReportService $reportService){
        $this->PermissionCheck($request, ['report delete']);
        return $reportService->destroy($id, $request);
    }
}
