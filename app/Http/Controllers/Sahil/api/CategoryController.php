<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Resource;
use App\Models\Category;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;

    public function index(Request $request){
        $this->PermissionCheck($request, ['category view']);
        $categories = Category::paginate(15);

        $data = Resource::collection($categories)->response()->getData(true);
        return $this->dataResponse('All Categories', $data);
    }

    public function show(Request $request, string $id){
        $this->PermissionCheck($request, ['category view']);
        $category = Category::find($id);
        if (!$category) {
            return $this->errorResponse('No Category For This ID: ' . $id, 404);
        }

        $data = new Resource($category);
        return $this->dataResponse('One Category', $data );
    }
}
