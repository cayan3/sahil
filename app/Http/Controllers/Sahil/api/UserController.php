<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\Resource;
use App\Models\User;
use App\Services\UserService;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;
    
    public function show(string $id, Request $request){
        $this->PermissionCheck($request, ['user create', 'user update']);
        $user = User::find($id);
        $data = new Resource($user);
        return $this->dataResponse('One User', $data);
    }

    public function update(UserRequest $request, string $id, UserService $userService){
        return $userService -> updateUser($request, $id);
    }

    public function destroy(string $id,Request $request,){
        $this->PermissionCheck($request, ['user delete']);
        $user = User::find($id);
        $user->delete();
        return $this->successResponse('User Deleted');
    }

    public function changeProfileImage(Request $request, UserService $userService) {
        return $userService -> changeProfileImage($request);
    }
}
