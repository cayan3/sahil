<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Resource;
use App\Models\Order;
use App\Models\Product;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Stripe\Checkout\Session;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Stripe\Charge;

class CheckoutController extends Controller
{
    use ResponseTrait;

    public function checkout_session(Request $request){
        $order = Order::find($request->orderId);
        if (!$order) {
            return $this -> errorResponse('Not Order For This ID: ' . $request->orderId, 404);
        }

        $checkout_session = $this->createSession($order);
        
        return $checkout_session;
    }

    public function success(Request $request) {
        Stripe::setApiKey(env('STRIPE_SECRET'));

        $sessionId = $request->query('session_id');
        try {
            $checkout_session = $this->retrieveSession($sessionId);
            $payment_intent = $this->retrievePaymentIntent($checkout_session->payment_intent);
            $charge = $this->retrieveCharge($payment_intent->latest_charge);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), 500);
        }

        $orderId = $checkout_session -> client_reference_id;
        $order = Order::findOrFail($orderId);

        $this->updateProductQuantityAndSold($order);

        $order->stripe_charge_id = $charge->id;
        $order -> is_paid = 1;
        $order -> save();

        return $this->successResponse('Order is paid');
    }

    public function cancel() {
        return $this->errorResponse('Somthing Went Wrong', 500);
    }

    private function createSession($order) {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $checkout_session = Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
            'price_data' => [
                'currency' => 'egp',
                'unit_amount' => $order->total_price * 100,
                'product_data' => [
                    'name' => 'The Price',
                ],
            ],
            'quantity' => 1,
        ]],
            'mode' => 'payment',
            'success_url' =>route('checkout-session.success') . '?session_id={CHECKOUT_SESSION_ID}',
            'cancel_url' => route('checkout-session.cancel'),
            'client_reference_id' => $order->id
        ]);

        return $checkout_session;
    }

    private function updateProductQuantityAndSold($order) {
        $orderProducts = $order -> orderProducts;
        $updateData = [];
        $quantityStatement = '';
        $soldStatement = '';

        foreach ($orderProducts as $orderProduct) {
            $quantity = $orderProduct->quantity;
            $id = $orderProduct->product_id;
            $updateData[] = [
                'id' => $id,
                'quantity' => $quantity,
            ];
            $quantityStatement .= "WHEN {$id} THEN quantity - {$quantity} ";
            $soldStatement .= "WHEN {$id} THEN sold + {$quantity} ";
        }

        $productIds = array_column($updateData, 'id');

        Product::whereIn('id', $productIds)
            ->update([
                'sold' => DB::raw("CASE id $soldStatement END"),
                'quantity' => DB::raw("CASE id $quantityStatement END"),
            ]);
    }

    private function retrieveSession($sessionId) {
        try {
            return Session::retrieve($sessionId);
        } catch (\Exception $e) {
            throw new \Exception('Unable to retrieve checkout session: ' . $e->getMessage());
        }
    }

    private function retrievePaymentIntent($paymentIntentId) {
        try {
            return PaymentIntent::retrieve($paymentIntentId);
        } catch (\Exception $e) {
            throw new \Exception('Unable to retrieve payment intent: ' . $e->getMessage());
        }
    }

    private function retrieveCharge($chargeId) {
        try {
            return Charge::retrieve($chargeId);
        } catch (\Exception $e) {
            throw new \Exception('Unable to retrieve charge: ' . $e->getMessage());
        }
    }
}
