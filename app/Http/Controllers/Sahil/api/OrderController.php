<?php

namespace App\Http\Controllers\Sahil\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Http\Resources\Resource;
use App\Models\Order;
use App\Services\OrderService;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;
    
    public function index(Request $request, OrderService $orderService){
        $this->PermissionCheck($request, ['order view']);
        $orders = $orderService -> search($request)->get();
        $data = Resource::collection($orders);
        return $this->dataResponse('Success', $data);
    }

    public function store(OrderRequest $request, OrderService $orderService){
        return $orderService->store($request);
    }

    public function show(Request $request, string $id, OrderService $orderService){
        $this->PermissionCheck($request, ['order view']);
        $order = $orderService -> getOneOrder($id);
        if(!$order){
            return $this->errorResponse('No Order For This ID: ' . $id, 404);
        }

        $data = new Resource($order);
        return $this->dataResponse('Order', $data);
    }

    public function cancel(Request $request, string $id) {
        $this->PermissionCheck($request, ['order cancel']);
        $order = Order::find($id);

        if($order->is_paid == 0 || $order->order_status != 'completed'){
            return $this->errorResponse("Can't Cancel This Order", 500);
        }
        if ($order->user_id != $request->user()->id) {
            return $this->errorResponse("This Order not belong to you", 406);
        }

        $order->order_status = 'canceled';
        $order->save();
    }

    public function destroy(Request $request, string $id, OrderService $orderService){
        $this->PermissionCheck($request, ['order delete']);
        $orderService -> destroy($id);
        return $this->successResponse('Order Deleted');
    }
}
