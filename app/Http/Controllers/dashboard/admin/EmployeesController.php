<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Mail\SendMail;
use App\Models\User;
use App\Services\EmployeeService;
use App\traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class EmployeesController extends Controller
{
    use HelperTrait;

    public function index(Request $request, EmployeeService $employeeService){
        return $employeeService->index($request);
    }

    public function create(EmployeeService $employeeService){
        return $employeeService->create();
    }

    public function store(EmployeeRequest $request, EmployeeService $employeeService){
        return $employeeService->store($request);
    }

    public function edit(string $id, EmployeeService $employeeService){
        return $employeeService->edit($id);
    }

    public function update(EmployeeRequest $request, string $id, EmployeeService $employeeService){
        return $employeeService->update($id, $request);
    }

    public function destroy(string $id, EmployeeService $employeeService){
        return $employeeService->destroy($id);
    }
}
