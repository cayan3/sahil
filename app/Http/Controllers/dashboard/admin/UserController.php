<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request){
        $users = User::where('role', Role::User);
        if (!empty($request->get('keyword'))) {
            $keyword = '%' . $request->get('keyword') . '%';
            $users = $users->where('name', 'like', $keyword);
        }

        return view('admin.users.list', [
            'users' => $users->paginate(10),
        ]);
    }
}
