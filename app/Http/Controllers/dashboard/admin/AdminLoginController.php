<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLoginRequest;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Services\AdminService;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    public function index(){
        return view('admin.login');
    }

    public function authenticate(AdminService $adminService, AdminLoginRequest $request){
        return $adminService -> authenticate($request);
    }
 
    public function dashboard(Request $request){
        $orders = Order::all();

        $sales = $orders->sum('total_price');

        $orderCount = $orders->count();

        $customersCount = User::where('role', Role::User)->get()->count();

        $productCount = null;
        if ($request->user()->role == Role::Vendor->value) {
            $products = Product::where('sold', '>', 0)
            ->where('vendor_id', $request->user()->vendor->id)->get();
            
            $sales = 0;
            $productCount = count($products);
            for ($i=0; $i < $productCount; $i++) {
                $saleOfProduct = $products[$i]->price * $products[$i]->sold;
                $sales += $saleOfProduct;
            }
        }

        return view('admin.dashboard', [
            'sales' => $sales,
            'orderCount' => $orderCount,
            'customersCount' => $customersCount,
            'productCount' => $productCount,
        ]);
    }

    public function logout(Request $request){
        $request->user()->tokens()->delete();
        return redirect()->route('login');    
    }
}
