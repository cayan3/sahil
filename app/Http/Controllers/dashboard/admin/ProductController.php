<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request, ProductService $productService){
        $data = $productService -> index($request);

        return view('admin.products.list',[
            'products' => $data -> products,
            'categories' => $data -> categories,
            'brands' => $data -> brands,
        ]);
    }

    public function create(ProductService $productService){
        return $productService->create();
    }

    public function store(ProductRequest $request, ProductService $productService){
        return $productService->store($request);
    }

    public function edit($productId,$productDetailId, ProductService $productService){
        return $productService->edit($productId,$productDetailId);
    }

    public function update($productId, $productDetailId, UpdateProductRequest $request, ProductService $productService){
        return $productService->update($productId,$productDetailId, $request);
    }

    public function destroy(Request $request, $productId, ProductService $productService){
        return $productService->destroy($request, $productId); 
    }

    public function SubCategoriesToCreateProduct(Request $request, ProductService $productService) {
        return $productService->SubCategoriesToCreateProduct($request);
    }

    public function SizesToCreateProduct(Request $request, ProductService $productService) {
        return $productService->SizesToCreateProduct($request);
    }
}
