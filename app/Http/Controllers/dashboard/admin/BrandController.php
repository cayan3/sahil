<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use App\Services\BrandService;
use App\traits\PermissionCheckTrait;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    use PermissionCheckTrait;

    public function index(Request $request, BrandService $brandService){
        $this->PermissionCheck($request, ['brand view']);
        return $brandService->index($request);
    }

    public function create(){
        return view('admin.brands.create');
    }

    public function store(BrandRequest $request,BrandService $brandService){
        return $brandService -> store($request);
    }

    public function edit($brandId){
        $brand = Brand::findOrFail($brandId);
        return view('admin.brands.edit',compact('brand'));
    }

    public function update($brandId, BrandRequest $request, BrandService $brandService){
        return $brandService->update($brandId,$request);
    }
    
    public function destroy(Request $request, $brandId, BrandService $brandService){
        $this->PermissionCheck($request, ['brand delete']);
        return $brandService -> destroy($brandId);
    }
}

