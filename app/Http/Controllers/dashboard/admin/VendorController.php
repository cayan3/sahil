<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\Controller;
use App\Services\VendorService;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    public function index(Request $request,VendorService $vendorService){
        $vendors = $vendorService -> search($request);

        return view('admin.vendors.list', [
            'vendors' => $vendors->paginate(10),
        ]);
    }

    public function destroy($vendorId, VendorService $vendorService){
        return $vendorService -> destroy($vendorId);
    }
}
