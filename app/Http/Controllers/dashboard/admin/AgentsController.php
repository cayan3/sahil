<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\AgentRequest;
use App\Mail\SendMail;
use App\Models\User;
use App\traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AgentsController extends Controller
{
     use HelperTrait;

    public function index(Request $request){
        $users = User::where('role', Role::Agent);
        if (!empty($request->get('keyword'))) {
            $keyword = '%' . $request->get('keyword') . '%';
            $users = $users->where('name', 'like', $keyword);
        }

        return view('admin.agents.list', [
            'agents' => $users->paginate(10),
        ]);
    }

    public function create(){
        return view('admin.agents.create');
    }

    public function store(AgentRequest $request){
        $password = $this -> CreateRandomCode();

        $data = $request->validated();
        $agent = User::create($data + [
            'password' => Hash::make($password),
            'new_password' => 1,
            'role' => Role::Agent,
        ]);

        $agent->assignRole($agent->role);

        Mail::to($request->email)->send(new SendMail($password,'Login With This Password'));

        return redirect()->route('agents.index')->with('success', 'Agent Created');
    }

    public function edit(string $id){
        $agent = User::findOrFail($id);

        return view('admin.agents.edit', [
            'agent' => $agent
        ]);
    }

    public function update(AgentRequest $request, string $id){
        $agent = User::findOrFail($id);

        $data = $request->validated();
        $agent->update($data);

        return redirect()->route('agents.index')->with('success', 'Agent Updated');
    }

    public function destroy(string $id){
        User::destroy($id);
        return redirect()->route('agents.index')->with('success', 'Agent Deleted');
    }
}
