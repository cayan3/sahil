<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UpdatePasswordRequest;
use App\Mail\SendMail;
use App\Models\User;
use App\Models\Verification_code;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PasswordController extends Controller
{
    public function changePassword()  {
        return view('admin.change-password');
    }

    public function updatePassword(ChangePasswordRequest $request) {

        /** @var \App\Models\User $user **/        
        $user = Auth::user();

        if (!Hash::check($request->currentPassword, $user->password)) {
            return redirect()->route('change.password')->with('error', "current password doesn't match");
        }

        $user->update([
            'password' => Hash::make($request->newPassword),
            'new_password' => 0
        ]);

        return redirect()->route('dashboard')->with('success', 'Password changed successfully');
    }

    public function forgotPassword() {
        return view('admin.password.enter-email');
    }

    public function enterOtp() {
        return view('admin.otp.enter');
    }

    public function enterPassword() {
        return view('admin.password.enter');
    }

    public function sendOtp(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
        ]);
        if ($validator->fails()) {
            return redirect()->route('forgot.password')->withErrors($validator->errors());
        }

        $user = User::where('email', $request->email)->first();

        $code = rand(100000, 999999);

        session(['email' => $user->email]);

        Verification_code::create([
            'code' => $code,
            'expire' => now()->addMinutes(10),
            'use_case' => 'reset password',
            'user_id' => $user->id,
        ]);

        Mail::to($request->email)->send(new SendMail($code, $request->use_case, 'Use this code to verify email'));

       return redirect()->route('enter.otp');
    }

    public function verifyOtp(Request $request) {
        $verificationCode = Verification_code::where('code' ,$request->otp)->first();

        if(
            $verificationCode && 
            now()->lessThanOrEqualTo($verificationCode->expire) && 
            $verificationCode->use_case == 'reset password'
            ){

            $verificationCode->delete();
            return redirect()->route('enter.password');
        }
        return redirect()->route('enter.otp')->with('error','Invalid Code');
    }

    public function resetPassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string',
        ]);
        if ($validator->fails()) {
            return redirect()->route('enter.password')->withErrors($validator->errors());
        }

        $email = session('email');
        $user = User::where('email', $email)->first();

        $user->update([
            'password' => Hash::make($request->password),
            'new_password' => 0
        ]);

        return redirect()->route('login')->with('success', 'Password changed successfully');
    }
}
