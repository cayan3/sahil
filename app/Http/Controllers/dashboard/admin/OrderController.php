<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\Controller;
use App\Services\OrderService;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;

    public function index(Request $request,OrderService $orderService){
        $this->PermissionCheck($request, ['order view']);
        $orders = $orderService -> search($request);

        return view('admin.orders.list', [
            'orders' => $orders->paginate(15),
        ]);
    }

    public function destroy(Request $request, $orderId, OrderService $orderService){
        $this->PermissionCheck($request, ['order delete']);
        $orderService -> destroy($orderId);
        return redirect()->route('orders.index')->with('success','Order Deleted');
    }
}
