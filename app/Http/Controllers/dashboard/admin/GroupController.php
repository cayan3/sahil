<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Services\GroupService;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    use ResponseTrait, PermissionCheckTrait;

    public function index(GroupService $groupService, Request $request){
        $this->PermissionCheck($request, ['group view']);
        $groups = $groupService->index();
        return view('admin.groups.list', [
            'groups' => $groups
        ]);
    }
}
