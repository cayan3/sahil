<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubCategoryRequest;
use App\Services\SubCategoryService;
use App\traits\PermissionCheckTrait;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    use PermissionCheckTrait;

    public function index(Request $request,SubCategoryService $subCategoryService){
        $this->PermissionCheck($request, ['sub_category view']);
        return $subCategoryService->index($request);
    }

    public function create(SubCategoryService $subCategoryService){
        return $subCategoryService->create();
    }

    public function store(SubCategoryRequest $request, SubCategoryService $subCategoryService){
        return $subCategoryService -> store($request);
    }

    public function edit($subCategoryId,SubCategoryService $subCategoryService){
        return $subCategoryService -> edit($subCategoryId);
    }

    public function update($subCategoryId, SubCategoryRequest $request, SubCategoryService $subCategoryService){
        return $subCategoryService -> update($subCategoryId,$request);
    }

    public function destroy(Request $request, $subCategoryId, SubCategoryService $subCategoryService){
        $this->PermissionCheck($request, ['sub_category delete']);
        return $subCategoryService -> destroy($subCategoryId);
    }
}

