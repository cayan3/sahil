<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Services\CategoryService;
use App\traits\PermissionCheckTrait;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use PermissionCheckTrait;

    public function index(Request $request,CategoryService $categoryService){
        $this->PermissionCheck($request, ['category view']);
        return $categoryService->index($request);
    }

    public function create(){
        return view('admin.category.create');
    }

    public function store(CategoryRequest $request,CategoryService $categoryService){
           return $categoryService->store($request);
    }

    public function edit($categoryId,Request $request){
        $category = Category::findOrFail($categoryId);
        return view('admin.category.edit',compact('category'));
    }

    public function update($categoryId,CategoryRequest $request,CategoryService $categoryService){
        return $categoryService->update($request, $categoryId);
    }

    public function destroy($categoryId, Request $request,CategoryService $categoryService){
        $this->PermissionCheck($request, ['category delete']);
       return $categoryService->destroy($categoryId,$request);
    }   
}