<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CouponRequest;
use App\Services\CouponService;
use App\traits\PermissionCheckTrait;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    use PermissionCheckTrait;

    public function index(Request $request, CouponService $couponService){
        $this->PermissionCheck($request, ['coupon view']);
        return $couponService -> index($request);
    }

    public function create(CouponService $couponService){
        return $couponService -> create();
    }

    public function store(CouponRequest $request, CouponService $couponService){
        return $couponService -> store($request);
    }

    public function edit($couponId, CouponService $couponService){
        return $couponService -> edit($couponId);
    }

    public function update($couponId,CouponRequest $request, CouponService $couponService){
        return $couponService -> update($couponId, $request);
    }

    public function destroy(Request $request, $couponId, CouponService $couponService){
        $this->PermissionCheck($request, ['coupon delete']);
        return $couponService -> destroy($couponId);
    }
}
