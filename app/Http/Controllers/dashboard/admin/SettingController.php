<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingRequest;
use App\Models\Setting;

class SettingController extends Controller
{
    public function index() {
        $setting = Setting::first();
        return view('admin.settings.list', compact('setting'));
    }

    public function createOrUpdate(SettingRequest $request) {
        $data = $request->validated();
        $setting = Setting::first();
        if ($setting) {

            $setting->update($data);

            if(!empty($request->site_logo)) {
                $setting -> getFirstMedia('site_logo') -> delete();
                $setting -> addMediaFromRequest('site_logo') -> toMediaCollection('site_logo');
            }

        } else {

            $setting = Setting::create($data);

            $setting -> addMediaFromRequest('site_logo') -> toMediaCollection('site_logo');
            
        }

        return redirect()->route('settings.index')->with('success', 'Settings updated successfully');
    }
}
