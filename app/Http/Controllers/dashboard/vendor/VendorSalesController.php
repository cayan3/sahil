<?php

namespace App\Http\Controllers\dashboard\vendor;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\traits\PermissionCheckTrait;
use Illuminate\Http\Request;

class VendorSalesController extends Controller
{
    use PermissionCheckTrait;

    public function index(Request $request) {
        $this->PermissionCheck($request, ['sales view']);

        $brands = Brand::latest()->get();
        $categories = Category::latest()->get();
        $products = Product::where('sold', '>', 0)
            ->where('vendor_id', $request->user()->vendor->id);            
        
        $products = $this->filters($request, $products);

        $totalSales = $this->totalSales($products);

        return view('vendor.sales',[
            'products' => $products->paginate(15),
            'categories' => $categories,
            'brands' => $brands,
            'totalSales' => $totalSales
        ]);
    }

    public function filters($request, $products) {
        if(!empty($request->get('category')) && !empty($request->get('sub_category'))){
            $sub_categoryId=$request->get('sub_category');
            $products=$products->where('sub_category_id',$sub_categoryId);
        }
        
        if(!empty($request->get('brand'))){
                $brandId=$request->get('brand');
                $products=$products->where('brand_id',$brandId);
        }

        if(!empty($request->get('keyword'))){
            $keyword = '%' . $request->get('keyword') . '%';
            $products=$products->where('title','like',$keyword);
        }
        
        return $products;
    }
    
    public function totalSales($products) {
        $totalSales = 0;
        $productCount = count($products->get());
        for ($i=0; $i < $productCount; $i++) {
            $saleOfProduct = $products->get()[$i]->price * $products->get()[$i]->sold;
            $totalSales += $saleOfProduct;
        }
        
        return $totalSales;
    }
}
