<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class IsVerified
{
    public function handle(Request $request, Closure $next): Response
    {
       if (Auth::check() && ! $request->user()->hasVerifiedEmail()) {
            return response()->json([
                'message' => 'Your email address is not verified.'
            ], 403);
        }

        return $next($request);
    }
}
