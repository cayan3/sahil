<?php

namespace App\traits;

use App\Models\Review;

trait ProductRatingTrait
{
    public function ProductRating($product) {        
        $reviews = Review::where('product_id', $product->id)->get();

        $rating = 0;
        for ($i=0; $i <count($reviews) ; $i++) { 
            $rating += $reviews[$i]->rating;
        }
        
        $reviewsCount = count($reviews);
        if ($reviewsCount == 0){
            $rating_average = 0;
        }else{
            $rating_average = $rating /count($reviews);
        }
        
        $product->rating_average = $rating_average;
        $product->save();
    }
}
