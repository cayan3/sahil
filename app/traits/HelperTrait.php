<?php

namespace App\traits;

trait HelperTrait
{
    public function CreateRandomCode() {
        $length = 6;
        $code = '';
        for ($i = 0; $i < $length; $i++) {
            $code .= random_int(0, 9);
        }
        return $code;
    }
}
