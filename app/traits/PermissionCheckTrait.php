<?php

namespace App\traits;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

trait PermissionCheckTrait
{
    public function PermissionCheck(Request $request, $permissions){
        foreach($permissions as $permission){
            if ($request->user()->can($permission)) {
                return true;
            };
        }
        throw new AuthorizationException('You are not authorized to access this action.');
    }
}