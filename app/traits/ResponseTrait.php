<?php

namespace App\traits;

trait ResponseTrait
{
    public function errorResponse($msg, $code)
    {
        return response()->json([
            'status' => false,
            'message' => $msg,
        ], $code);
    }

    public function successResponse($msg, $code = 200)
    {
        return response()->json([
            'status' => true,
            'message' => $msg,
        ], $code);
    }

    public function dataResponse($msg, $data, $code = 200)
    {
        return response()->json([
            'status' => true,
            'message' => $msg,
            'data' => $data,
        ], $code);
    }
}
