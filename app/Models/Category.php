<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Category extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = ['title','image','status'];

    public function subCategories() {
        return $this -> hasMany(SubCategory::class);
    }

    public function products() {
        return $this -> hasMany(Product::class);
    }

    public function size() {
       return $this->hasMany(Size::class); 
    }
}
