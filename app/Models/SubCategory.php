<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class SubCategory extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = ['title','status','category_id','image'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeBySubcategoryTitleOrCategoryTitle($query, $keyword)
    {
        return $query->where('title', 'like', '%' . $keyword . '%')
            ->orWhereHas('category', function ($query) use ($keyword) {
                $query->where('title', 'like', '%' . $keyword . '%');
            });
    }
}
