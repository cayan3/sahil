<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use HasFactory;

    protected $fillable = [
    'description',
    'evaluation',
    'average_shipping',
    'user_id'
];

    protected static function newFactory()
    {
        //return VendorFactory::new();
    }
    
    public function user() {
        return $this -> belongsTo(User::class);
    }
}
