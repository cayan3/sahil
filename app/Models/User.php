<?php
 
namespace App\Models;
  
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, Notifiable, InteractsWithMedia, HasRoles;

    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'phone',
        'provider_id',
        'provider',
        'new_password'
    ];
 
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
 
    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }

    public function cart() {
        return $this -> hasOne(Cart::class);
    }

    public function report() {
        return $this->hasMany(Report::class);
    }

    public function reviews() {
        return $this -> hasMany(Review::class);
    }

    public function vendor() {
        return $this -> hasOne(Vendor::class);
    }
    
    public function orders() {
        return $this -> hasMany(Order::class);
    }

    public function address() {
        return $this -> hasMany(Address::class);
    }

    public function payments() {
        return $this -> hasMany(Agent_Payments::class);
    }
}