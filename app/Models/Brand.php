<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Brand extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $keyType = 'string';
    
    protected $fillable = ['title','logo','status'];

    protected static function newFactory()
    {
        //return BrandFactory::new();
    }

    public function products() {
        return $this -> hasMany(Product::class);
    }
}

