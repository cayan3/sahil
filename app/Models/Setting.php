<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Setting extends Model implements HasMedia
{
    use InteractsWithMedia;
    
    protected $fillable = [
                'site_name',
                'site_email',
                'facebook_url',
                'twitter_url',
                'instagram_url',
                'commission',
                'commission_status',
                'shipping_fees',
                'shipping_fees_status',
            ];

    use HasFactory;

}
