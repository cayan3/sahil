<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Verification_code extends Model
{
    use HasFactory;

    protected $fillable = ['code','expire','use_case','user_id'];

    public function user() {
        return $this -> belongsTo(User::class);
    }
}
