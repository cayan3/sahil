<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Return_Products extends Model
{
    use HasFactory;

    protected $table = 'return_products';

    protected $fillable = [
        'product_id',
        'order_id',
        'quantity',
        'unit_price'
    ];

    public function return() {
        return $this->belongsTo(Returns::class);
    }

    public function product() {
        return $this->hasMany(Product::class);
    }
}
