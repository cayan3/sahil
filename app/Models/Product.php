<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Product extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'title',
        'image_cover',
        'description',
        'sold',
        'price',
        'quantity',
        'brand_id',
        'sub_category_id',
        'vendor_id',
        'category_id',
    ];

    public function categories() {
        return $this -> belongsTo(Category::class);
    }

    public function subCategories() {
        return $this -> belongsTo(Category::class);
    }

    public function brands() {
        return $this -> belongsTo(Category::class);
    }

    public function favorite() {
        return $this -> belongsTo(Favorite::class);
    }

    public function cartProduct() {
        return $this -> hasMany(Cart_Products::class);
    }

    public function orderProducts() {
        return $this->hasMany(Order_Products::class);
    }

    public function returnProducts() {
        return $this->belongsTo(Return_Products::class);
    }

    public function reports() {
        return $this->hasMany(Report::class);
    }

    public function reviews() {
        return $this -> hasMany(Review::class);
    }

    public function productDetails() {
        return $this -> hasMany(ProductDetail::class);
    }
}

