<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Returns extends Model
{
    use HasFactory;

    protected $fillable = ['code', 'order_id', 'return_reason', 'return_status', 'user_id'];

    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function returnProducts() {
        return $this->hasMany(Return_Products::class);
    }
}
