<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'commission',
        'total_price',
        'recieved_price',
        'status',
        'agent_id',
        'employee_id'
    ];

    public function agent() {
        return $this->belongsTo(User::class,'agent_id');
    }
    
    public function employee() {
        return $this->belongsTo(User::class,'employee_id');
    }

    public function orders() {
        return $this->hasMany(Order::class);
    }
    
    public function payments() {
        return $this->hasMany(Agent_Payments::class);
    }
}
