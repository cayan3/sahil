<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = ['status','code','discount','expire'];

    protected static function newFactory()
    {
        //return CouponFactory::new();
    }

    // public function orders() {
    //     return $this -> belongsTo(Order::class);
    // }
}

