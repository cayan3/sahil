<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order_Products extends Model
{
    use HasFactory;

    protected $table = 'order_products';
    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'product_details_id'
    ];
    
    public function order() {
        $this -> belongsTo(Order::class);
    }

    public function product() {
        $this -> hasMany(Product::class);
    }

    public function productDetails() {
        return $this -> hasMany(ProductDetail::class);
    }
}
