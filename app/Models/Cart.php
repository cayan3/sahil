<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'unit_price'];

    public function cartProducts() {
        return $this -> hasMany(Cart_Products::class);
    }

    public function user() {
        return $this -> belongsTo(User::class);
    }
}
