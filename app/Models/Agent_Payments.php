<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agent_Payments extends Model
{
    use HasFactory;

    protected $table = 'agent_payments';

    protected $fillable = [
        'amount_recieved',
        'amount_not_recieved',
        'agent_id',
        'group_id',
    ];

    public function agent() {
        return $this->belongsTo(User::class);
    }
    
    public function group() {
        return $this->belongsTo(Group::class);
    }
}
