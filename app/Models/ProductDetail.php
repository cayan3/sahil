<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    use HasFactory;

    protected $table = 'product_details';
    protected $fillable = ['product_id', 'size_id', 'color_id', 'price'];


    public function color() {
        return $this->belongsTo(Color::class);
    }

    public function size() {
        return $this->belongsTo(Size::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function cartProducts() {
        return $this->hasMany(Cart_Products::class);
    }

    public function orderProducts() {
        return $this->hasMany(Order_Products::class);
    }
}
