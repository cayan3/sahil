<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'order_status',
        'total_price',
        'payment_method',
        'shipping_fees',
        'expected_date',
        'discount_amount',
        'is_paid',
        'coupon_id',
        'address_id',
        'user_id',
    ];

    public function orderProducts() {
        return $this -> hasMany(Order_Products::class);
    }

    public function user() {
        return $this -> belongsTo(User::class);
    }

    public function address() {
        return $this -> belongsTo(Address::class);
    }

    public function coupon() {
        return $this -> belongsTo(Coupon::class);
    }

    public function return() {
        return $this->hasOne(Returns::class);
    }
}
