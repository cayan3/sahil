<?php

namespace App\Services;

use App\Enums\Role;
use App\Models\User;
use App\Models\Vendor;
use App\traits\ResponseTrait;

class VendorService
{
    use ResponseTrait;

    public function createVendor($data){
        return Vendor::create($data);
    }
    
    public function search($request) {
        $vendors = Vendor::latest();
        if (!empty($request->get('keyword'))) {
            $keyword = '%' . $request->get('keyword') . '%';
            $vendors = $vendors->where('name', 'like', $keyword);
        }
        return $vendors;
    }

    public function destroy($vendorId){
        $vendor = Vendor::find($vendorId);
        User::destroy($vendor->user_id);
        return redirect()->route('vendors.index')->with('success','Vendor Deleted');
    }
}
