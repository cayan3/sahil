<?php

namespace App\Services;

use App\Http\Resources\Resource;
use App\Models\Group;
use App\Models\Order;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class AgentOrderService
{
    use ResponseTrait;

    public function getCurrentOrders(Request $request) {
        $agent = $request->user();
        $group = Group::where('agent_id', $agent->id)->where('status', 'processing')->first();
        $orders = Order::where('group_id', $group->id)->where('order_status', 'processing')->get();

        $data = Resource::collection($orders);
        return $this->dataResponse('Current Orders', $data);
    }

    public function getPreviousOrders(Request $request){
        $agent = $request->user();
        $groups = Group::where('agent_id', $agent->id)->get();
        
        $groupIds = [];
        for ($i=0; $i < count($groups) ; $i++) { 
            $groupIds[] = [$groups[$i]->id];
        }

        $orders = Order::where('group_id', $groupIds)->where('order_status', 'completed')->get();
       
        $data = Resource::collection($orders);
        return $this->dataResponse('Previous Orders', $data);
    }

    public function getCanceledOrders(Request $request){
        $agent = $request->user();
        $groups = Group::where('agent_id', $agent->id)->get();
        
        $groupIds = [];
        for ($i=0; $i < count($groups) ; $i++) { 
            $groupIds[] = [$groups[$i]->id];
        }

        $orders = Order::where('group_id', $groupIds)->where('order_status', 'canceled')->get();
       
        $data = Resource::collection($orders);
        return $this->dataResponse('Canceled Orders', $data);
    }
    
    public function setOrderComplete($id, Request $request) {
        $agent = $request -> user();
        $group = Group::where('agent_id', $agent->id)->where('status', 'processing')->first();

        $order = Order::find($id);

        if (!$order) {
            return $this->errorResponse('No order for this ID: ' . $id, 404);
        }
        
        if ($order->group_id != $group->id) {
            return $this->errorResponse('This order not belong your group', 500);
        }

        $order -> order_status = 'completed';
        $order -> is_paid = 1;
        $order -> save();

        return $this->successResponse('Success');
    }

    public function setOrderCancel($id, Request $request) {
        $agent = $request -> user();
        $group = Group::where('agent_id', $agent->id)->where('status', 'processing')->first();

        $order = Order::find($id);

        if (!$order) {
            return $this->errorResponse('No order for this ID: ' . $id, 404);
        }
        
        if ($order->group_id != $group->id) {
            return $this->errorResponse('This order not belong your group', 500);
        }

        $order -> order_status = 'canceled';
        $order -> save();

        return $this->successResponse('Order Canceled');
    }

    public function show($id) {
        $order = Order::find($id);
        if (!$order) {
            return $this->errorResponse('No order for this ID: ' . $id, 404);
        }
        $data = new Resource($order);
        return $this->dataResponse('Order', $data);
    }
}