<?php
namespace App\Services;

use App\Http\Requests\OrderRequest;
use App\Http\Resources\Resource;
use App\Models\Address;
use App\Models\Cart;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Order_Products;
use App\Models\Setting;
use App\traits\HelperTrait;
use App\traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class OrderService
{
    use ResponseTrait, HelperTrait;

    public function store(OrderRequest $request) {
        $cart = Cart::where('user_id', auth()->id())->first();
        if (!$cart) {
            return $this->errorResponse('No Cart For This User', 404);
        }

        $address = Address::find($request -> address_id);
        if (!$address) {
            return $this->errorResponse('No Address For This Id', 404);
        }
        
        DB::beginTransaction();
        try {
            $order = $this->createOrder($request, $cart);

            $this -> createOrderProducts($cart, $order->id);
            
            $cart->delete();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse('Order creation failed. Please try again. ' . $e, 500);
        }

        $data = new Resource($order);
        return $this->dataResponse('Order created successfully', $data);
    }

    public function search($request) {
        $orders = Order::latest();
        if (!empty($request->get('keyword'))) {
            $keyword = '%' . $request->get('keyword') . '%';
            $orders = $orders->where('code', 'like', $keyword);
        }
        return $orders;
    }

    public function getOneOrder($id){
        $order = Order::find($id);
        
        return $order;
    }
    
    public function destroy($id){
        $order = Order::where('id', $id)->where('user_id', auth()->user()->id)->delete();
        if (!$order) {
            return $this->errorResponse("You can't delete this order", 500);
        }
    }

    private function createOrderProducts($cart, $orderId) {
        $cartProducts = $cart -> cartProducts;
            $orderProducts = [];
            foreach ($cartProducts as $cartProduct) {
                $orderProducts[] = [
                    'order_id' => $orderId,
                    'product_id' => $cartProduct->product_id,
                    'product_details_id' => $cartProduct->product_details_id,
                    'quantity' => $cartProduct->quantity,
                ];
            }
            Order_Products::insert($orderProducts);
    }

    private function createOrder($request, $cart) {
        $code = $this->CreateRandomCode();

        $discount_amount = 0;

        $orderData = $request->validated();
        $orderData ['user_id'] =  auth()->id();

        $total_price = $cart -> total_price;

        if($request->coupon_id){
            $coupon = Coupon::find($request->coupon_id);

            if($coupon->status == 0){
                return $this->errorResponse('This Coupon Is Inactive', 406);
            }

            if($coupon->expire < now()){
                return $this->errorResponse('Coupon Is Expired', 406);
            }

            $discount_amount = $cart -> total_price * $coupon->discount / 100;

            $total_price = $total_price - $discount_amount;

            $orderData ['discount_amount'] = $discount_amount;   
        }

        $shippingFees = $this->getShippingFees($total_price);
        $orderData ['shipping_fees'] = $shippingFees;
        
        $total_price = $total_price + $shippingFees;

        $orderData ['total_price'] = $total_price;

        $order = Order::create($orderData + [
            'code' => $code,
        ]);

        return $order;
    }

    private function getShippingFees($total_price) {
        $settings = Setting::first();
        $shippingFees = $settings->shipping_fees;
        $shippingFeesStatus = $settings->shipping_fees_status;

        if($shippingFeesStatus == 'percentage'){
            $shippingFees = $total_price * $shippingFees / 100;
        }

        return $shippingFees;
    }
}