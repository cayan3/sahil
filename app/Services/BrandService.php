<?php

namespace App\Services;

use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandService
{
    public function index(Request $request){
        $brands = Brand::latest();
        if(!empty($request->get('keyword'))){
            $keyword= '%' . $request->get('keyword') . '%';
            $brands = $brands->where('title','like',$keyword);
        }
        return view('admin.brands.list',['brands' => $brands->paginate(10)]);
    }

    public function store(BrandRequest $request){
        $data = $request->only(['title', 'status']);
        $brand = Brand::create($data);

        $brand->addMediaFromRequest('logo')->toMediaCollection('brands');

        return redirect()->route('brands.index')->with('success','Brands Created');
    }

    public function update($brandId, BrandRequest $request){
        $brand = Brand::findOrFail($brandId);

        if(!empty($request->logo)){
            $brand -> getFirstMedia('brands') -> delete();
            $brand -> addMediaFromRequest('logo') -> toMediaCollection('brands');
        }

        $data = $request->only(['title', 'status']);
        $brand -> update($data);

        return redirect()->route('brands.index')->with('success','Brands Updated');
    }

    public function destroy($brandId){
        $brand = Brand::findOrFail($brandId);
        $brand->delete();

        return redirect()->route('brands.index')->with('success','Brand Deleted');
    }
}
