<?php

namespace App\Services;

use App\Http\Requests\ReportRequest;
use App\Http\Resources\Resource;
use App\Models\Report;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class ReportService
{
    use ResponseTrait;

    public function getAllReportsForSpecificUser(Request $request) {
        $reports = Report::where('user_id', $request -> user()->id)->get();
        
        $data = Resource::collection($reports);
        return $this->dataResponse('All Reports', $data);
    }

    public function store(ReportRequest $request) {
        $report = $this->createReport($request);       

        $data = new Resource($report);
        return $this->dataResponse('Report', $data);
    }

    public function show($id) {
        $report = Report::find($id);
        if (!$report) {
            return $this->errorResponse('No Report For This ID: ' . $id, 404);
        }
        $data = new Resource($report);
        return $this->dataResponse('Report', $data);
    }

    public function update($id, ReportRequest $request) {
        $report = Report::find($id);
        if (!$report) {
            return $this->errorResponse('No Report For This ID: ' . $id, 404);
        }
        $report->update([
            'product_id' => $request->productId,
            'report_data' => $request->reportData,
        ]);

        $data = new Resource($report);
        return $this->dataResponse('Report Updated', $data);
    }

    public function destroy($id, Request $request) {
        $report = Report::find($id);
        if (!$report) {
            return $this->errorResponse('No Report For This ID: ' . $id, 404);
        }

        if ($report->user_id != $request->user()->id) {
            return $this->errorResponse('This Report not belonge to this user', 500);
        }

        $report->delete();
        return $this->successResponse('Report Deleted');
    }

    private function createReport($request) {
        $reportData = $request->validated();
        $reportData ['product_id'] = $request -> productId;
        $reportData ['report_data'] = $request -> reportData;

        $report = Report::create($reportData + [
            'user_id' => auth()->id(),
        ]);

        return $report;
    }
}