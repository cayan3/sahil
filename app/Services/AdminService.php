<?php

namespace App\Services;

use App\Enums\Role;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AdminLoginRequest;

class AdminService
{
    public function authenticate(AdminLoginRequest $request){
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            if (Auth::user()->role == Role::Admin->value || Auth::user()->role == Role::Vendor->value) {
                return redirect()->route('dashboard');
            }
            Auth::logout();
            return redirect()
                    ->route('login')
                    ->with('error', 'You are not authorized to access the admin dashboard.');
        }
        return redirect()->route('login')->with('error', 'Error in Email Or Password.');
    }
}