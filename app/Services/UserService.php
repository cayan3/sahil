<?php

namespace App\Services;

use App\Enums\Role;
use App\Http\Resources\Resource;
use App\Models\User;
use App\traits\ResponseTrait;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserService
{
    use ResponseTrait;

    public function createUser($request, $role = Role::User){
        $userData = $request->validated();
        $userData['password'] = Hash::make($request -> password);
        $user = User::create($userData + [
            'role' => $role,
        ]);

        $user->assignRole($role);
        return $user;
    }

    public function updateUser($request, $id){
        $user = User::find($id);
        if(!$user){
            return $this->errorResponse('User Not Found', 404);
        }

        $userData = $request->only(['name', 'email', 'phone']);
        $user->update($userData);

        $data = new Resource($user);
        return $this->dataResponse('User Updated', $data);
    }

    public function getUserByEmail($email) {
        $user = User::where('email', $email)->first();
        return $user;
    }

    public function changeProfileImage($request) {
        $validator = Validator::make($request->all(),[
            'profileImage' => 'required|image|mimes:jpeg,png,jpg,gif|max:10240'
        ]);

        if ($validator->fails()) {
            return response()->json([
            'message' => 'Validation failed',
            'errors' => $validator->errors(),
            ], 422);
        }

        $user = $request->user();

        $user -> getFirstMedia('users') -> delete();
            
        $user -> addMediaFromRequest('profileImage') -> toMediaCollection('users');

        return $this->successResponse('Profile Image Updated');
    }
}
