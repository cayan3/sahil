<?php

namespace App\Services;

use App\Enums\Role;
use App\Http\Requests\GroupRequest;
use App\Http\Resources\Resource;
use App\Models\Group;
use App\Models\Order;
use App\Models\Setting;
use App\Models\User;
use App\traits\HelperTrait;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class GroupService
{
    use ResponseTrait, HelperTrait, PermissionCheckTrait;

    public function index(){
        $groups = Group::paginate(10);

        return $groups;
    }

    public function store(GroupRequest $request){
        $agent = User::where('id', $request->agent_id)->where('role', Role::Agent)->first();

        if (!$agent) {
            return $this->errorResponse('No agent for this id: ' . $request->agent_id, 404);
        }

        if ($this->checkProcessingGroup($agent)) {
            return $this->errorResponse("You can't create new group because the agent have processing group", 500);
        }

        $orders = Order::whereIn('id', $request->orders);

        $group = $this->createGroup($orders, $agent);

        $orders->update([
            'order_status' => 'processing',
            'group_id' => $group->id
        ]);

        $data = new Resource($group);
        return $this->dataResponse('Group Created', $data);
    }

    public function show(string $id, Request $request){
        $this->PermissionCheck($request, ['group view']);
        $group = Group::with('orders')->find($id);

        if (!$group) {
            return $this->errorResponse('No group for this ID: ' . $id, 404);
        }

        $data = new Resource($group);
        return $this->dataResponse("Success", $data);
    }

    public function update($groupId, GroupRequest $request){
        $group = Group::find($groupId);
        if (!$group) {
            return $this->errorResponse('No group for this id: ' . $groupId, 404);
        }

        $orders = Order::whereIn('id', $request->orders);
        $orders->update([
            'order_status' => 'processing',
            'group_id' => $group->id
        ]);

        $total_price = 0;
        $recieved_price = 0;
        foreach($orders->get() as $order){
            if ($order->is_paid) {
                $recieved_price += $order->total_price;
            }
            $total_price += $order->total_price;
        }

        $group->recieved_price = $recieved_price;
        $group->total_price = $total_price;
        $group->agent_id = $request->agent_id;
        $group->commission = $request->commission;
        $group->save();

        $data = new Resource($group);
        return $this->dataResponse('Group Updated', $data);
    }

    public function destroy($groupId){
       $group = Group::destroy($groupId);
        if (!$group) {
            return $this->errorResponse('No group for this id: ' . $groupId, 404);
        }
        return $this->successResponse('Group Deleted');
    }

    public function updateGroupStatus($groupId, $request) {
        $group = Group::find($groupId);
        if (!$group) {
            return $this->errorResponse('No group for this id: ' . $groupId, 404);
        }

        $group->status = $request->status;
        $group->save();
        return $this->successResponse('Group Status Updated');
    }

    public function totalAmount(Request $request) {
        $this->PermissionCheck($request, ['totalAmount view']);
        $agent = $request->user();

        $group = Group::where('agent_id', $agent->id)->where('status', 'processing')->first();
        
        if (!$group) {
            return $this->errorResponse("You don't have any groups recently", 404);
        }
        
        $totalAmount = $group -> not_recieved_price;

        $data = ['totalAmount' => $totalAmount];
        return $this->dataResponse('Total Amount', $data);
    }

    public function commission(Request $request) {
        $this->PermissionCheck($request, ['commission view']);
        $agent = $request->user();

        $group = Group::where('agent_id', $agent->id)->where('status', 'processing')->first();

        if (!$group) {
            return $this->errorResponse("You don't have any groups recently", 404);
        }

        $commission = 
            $group -> commission * ($group -> not_recieved_price + $group -> recieved_price) / 100;

        $data = ['commission' => $commission ];
        return $this->dataResponse('Your Commission', $data);
    }

    private function checkProcessingGroup($agent) {
        $existGroup = Group::where('agent_id', $agent->id)->where('status', 'processing')->first();
        if ($existGroup) {
            return true;
        }
        return false;
    }

    private function createGroup($orders, $agent) {
        $not_recieved_price = 0;
        $recieved_price = 0;
        foreach($orders->get() as $order){
            if ($order->is_paid) {
                $recieved_price += $order->total_price;
            }else{
                $not_recieved_price += $order->total_price;
            }
        }

        $commission = $this->getCommission($not_recieved_price + $recieved_price);
        $code = $this->CreateRandomCode();

        $group = Group::create([
            'code' => $code,
            'commission' => $commission,
            'agent_id' => $agent->id,
            'employee_id' => auth()->id(),
            'recieved_price' => $recieved_price,
            'not_recieved_price' => $not_recieved_price
        ]);

        return $group;
    }

    private function getCommission($totalPrice) {
        $settings = Setting::first();
        $commission = $settings->commission;
        $commissionStatus = $settings->commission_status;

        if ($commissionStatus == 'percentage') {
            $commission = $totalPrice * $commission / 100;
        }

        return $commission;
    }
}
