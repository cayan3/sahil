<?php

namespace App\Services;

use App\Enums\Role;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\Size;
use App\Models\SubCategory;
use App\Models\Vendor;
use App\traits\PermissionCheckTrait;
use Illuminate\Http\Request;

class ProductService
{
    use PermissionCheckTrait;

    public function index(Request $request){
        $this->PermissionCheck($request, ['product view']);
        
        $brands = Brand::latest()->get();
        $categories = Category::latest()->get();
        $products = Product::with('productDetails')->latest()->where('status', 1);

        $products = $this->vendorProducts($request, $products);

        $products = $this->filters($request, $products);

        return (object) [
            'products' => $products->paginate(15),
            'categories' => $categories,
            'brands' => $brands,
        ];
    }

    public function create(){
        $categories = Category::orderBy('title','ASC')->get();
        $brands = Brand::orderBy('title','ASC')->get();
        $colors = Color::all();

        return view('admin.products.create',[
            'categories' => $categories,
            'brands' => $brands,
            'colors' => $colors
        ]);
    }

    public function store(ProductRequest $request){
        $subCategory = SubCategory::find($request->sub_category_id);
        $brand = Brand::find($request->brand_id);
        
        if ($this->isInactive($subCategory, 'Sub Category') || $this->isInactive($brand, 'Brand')) {
            return redirect()->route('products.index');
        }
        
        $vendorId = $this->getVendorId($request);    

        $data = $request->only(['title','quantity','description', 'status', 'sub_category_id', 'brand_id', 'category_id']);
        $product = Product::create($data + [
            'vendor_id' => $vendorId,
        ]);
        
        $productDetailData = $request->only(['color_id','size_id', 'price']);
        ProductDetail::create($productDetailData + [
            'product_id' => $product->id,
        ]);

        $product -> addMediaFromRequest('imageCover')->toMediaCollection('products');
       
        return redirect()->route('products.index')->with('success','Product Created');
    }

    public function edit($productId,$productDetailId){
        $categories = Category::orderBy('title','ASC')->get();
        $brands = Brand::orderBy('title','ASC')->get();
        $product = Product::findOrFail($productId);
        $colors = Color::all();
        $productDetail = ProductDetail::where('product_id', $productId)
            ->where('id', $productDetailId)->first();
        
        return view('admin.products.edit',[
            'categories' => $categories,
            'brands' => $brands,
            'product' => $product,
            'productDetail' => $productDetail,
            'colors' => $colors
        ]);
    }

    public function update($productId, $productDetailId, UpdateProductRequest $request){
        $product = Product::findOrFail($productId);
        if($request->user()->role == Role::Admin->value || $product->vendor_id = $request->user()->vendor->id){
            
            $this->setProductImage($request, $product);

            $productData = $request->only(['title','quantity','description','status','category_id','sub_category_id','brand_id']);
            $product->update($productData);

            $productDetail = ProductDetail::find($productDetailId);

            $productUpdatedData = $request->only(['color_id','size_id','price']);
            $productDetail->update($productUpdatedData);

            return redirect()->route('products.index')->with('success','Product Updated');
        }else{
            return redirect()->route('products.index')->with('error','This product does not belong to you');
        }
    }

    public function destroy($request, $productId){
        $this->PermissionCheck($request, ['product delete']);
        $product = Product::find($productId);
        
        if(auth()->user()->role == Role::Admin->value || $product->vendor_id = auth()->user()->vendor->id){
            $product->delete();
            
            return redirect()->route('products.index')->with('success','Product Deleted');
        }
        return redirect()->route('products.index')->with('error','This product does not belong to you');
    }

    public function SubCategoriesToCreateProduct(Request $request) {
        if (!empty($request->categoryId)){
            $subCategories = SubCategory::where('category_id',$request->categoryId)->orderBy('title','ASC')->get();
            return response()->json([
                'status' => true,
                'subCategories' => $subCategories
            ]);
        }else {
            return response()->json([
                'status' => false,
                'subCategories' => []
            ]);
        }
    }

    public function SizesToCreateProduct(Request $request) {
        if (!empty($request->categoryId)){
            $sizes = Size::where('category_id',$request->categoryId)->get();
            return response()->json([
                'status' => true,
                'sizes' => $sizes
            ]);
        }else {
            return response()->json([
                'status' => false,
                'sizes' => []
            ]);
        }
    }
    
    public function getVendorId($request){
        $vendorId = null;
        if($request->user()->role == Role::Vendor->value){
            $vendor = Vendor::where('user_id', $request->user()->id)->first();
            $vendorId = $vendor->id;
        }
        return $vendorId;
    }

    public function isInactive($model, $type){
        if ($model->status == 0) {
            session()->flash('error', "This $type is inactive");
            return true;
        }
        return false;
    }

    public function filters($request, $products) {
        if(!empty($request->get('category')) && !empty($request->get('sub_category'))){
            $sub_categoryId=$request->get('sub_category');
            $products=$products->where('sub_category_id',$sub_categoryId);
        }
        
        if(!empty($request->get('brand'))){
                $brandId=$request->get('brand');
                $products=$products->where('brand_id',$brandId);
        }

        if(!empty($request->get('keyword'))){
            $keyword = '%' . $request->get('keyword') . '%';
            $products=$products->where('title','like',$keyword);
        }
        
        return $products;
    }
    
    public function vendorProducts($request, $products) {
        if ($request->user()->role == Role::Vendor->value) {
            $products= $products->where('vendor_id',$request->user()->vendor->id);
        }
        return $products;
    }

    public function setProductImage($request, $product) {
        if (!empty($request->imageCover)){
            if ($media = $product->getFirstMedia('products')) {
                $media->delete();
            }
            $product -> addMediaFromRequest('imageCover')->toMediaCollection('products');
        }
    }
}
