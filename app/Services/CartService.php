<?php

namespace App\Services;

use App\Http\Requests\CartRequest;
use App\Http\Resources\Resource;
use App\Models\Cart;
use App\Models\Cart_Products;
use App\Models\Product;
use App\Models\ProductDetail;
use App\traits\ResponseTrait;

class CartService {
    use ResponseTrait;

    public function store(CartRequest $request) {
        $cart = $this->createCartIfNotExist($request);        

        $product = Product::find($request->productId);
        
        $productDetail = ProductDetail::find($request->productDetailsId);

        $cart->total_price = ($cart->total_price + ($request->quantity * $productDetail->price));
        $cart->save();

        if ($request->quantity > $product->quantity) {
            return $this -> errorResponse('This quantity is not found', 500);
        }

        $this->createCartProductIfNotExist($request, $cart, $product);
        
        return $this -> successResponse('Add Product Success');
    }

    public function update($request, $id) {
        $cart = Cart::with('cartProducts')->find($id);

        if(!$cart){
            return $this -> errorResponse('No Cart For This ID: ' . $id, 404);
        }

        $this->updateCartItems($request, $cart);

        $totalPrice = $this -> calcPrice($cart);
        $cart->total_price = $totalPrice;
        $cart->save();

        $data = new Resource($cart);
        return $this -> dataResponse('Cart Updated', $data);
    }

    public function destroy($id) {
        $cart = Cart::destroy($id);
        if (!$cart) {
            return $this -> errorResponse('Not Cart For This ID: ' . $id, 404);
        }
        return $this -> successResponse('Cart Deleted');
    }

    public function calcPrice($cart) {
        $totalPrice = 0;
        foreach($cart -> cartProducts as $cartProduct){
            $totalPrice += $cartProduct->quantity * $cartProduct->productDetails->price;
        }
        return $totalPrice;
    }

    public function getOneCart($id) {
        $cart = Cart::find($id)->with('cartProducts')->get();
        if (!$cart) {
            return $this -> errorResponse('Not Cart For This ID: ' . $id, 404);
        }

        $data = new Resource($cart);
        return $this->dataResponse('Success', $data);
    }

    public function createCartIfNotExist($request) {
        $user = $request->user();
        $cart = $user->cart;
        if (!$cart) {
            $cart = Cart::create([
                'user_id' => $user->id,
            ]);
        }
        return $cart;
    }

    public function createCartProductIfNotExist($request, $cart, $product) {
        $existCartProduct = Cart_Products::where('product_id' ,$request->productId)
                                        ->where('cart_id',$cart->id)->first();
        if($existCartProduct){
            $existCartProduct->quantity += $request->quantity;

            $existCartProduct->save();
        }else{
             Cart_Products::create([
                'cart_id' => $cart->id,
                'product_id' => $product->id,
                'product_details_id' => $request->productDetailsId,
                'quantity' => $request->quantity,
            ]);
        }
    }

    public function updateCartItems($request, $cart) {
        $data = $request->validated();

        foreach ($data as $item) {
            $cartProduct = $cart->cartProducts()
                ->where('product_id', $item['productId'])
                ->where('product_details_id', $item['productDetailsId'])
                ->first();

            if ($cartProduct) {
                $cartProduct->quantity = $item['quantity'];
                $cartProduct->save();
            } else {
                $cart->cartProducts()->create([
                    'product_id' => $item['productId'],
                    'product_details_id' => $item['productDetailsId'],
                    'quantity' => $item['quantity'],
                ]);
            }
        }
    }
}