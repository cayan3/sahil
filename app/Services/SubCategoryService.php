<?php

namespace App\Services;

use App\Http\Requests\SubCategoryRequest;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class SubCategoryService
{
    public function index(Request $request){ 
        $subCategories = SubCategory::query();

        $keyword = $request->input('keyword');

        if (!empty($keyword)) {
            $subCategories->bySubcategoryNameOrCategoryTitle($keyword);
        }

        $subCategories = $subCategories->latest('id');

        return view('admin.sub_category.list', [
        'subCategories' => $subCategories->paginate(10),
        ]);
    }

    public function create(){
        $categories = Category::orderBy('title','ASC')->get();

        return view('admin.sub_category.create',compact('categories'));
    }

    public function store(SubCategoryRequest $request){
        $category = Category::find($request->category_id);

        if ($category->status == 0) {
            return redirect()->route('subcategories.index')->with('error','This Category is inactive');
        }

        $data = $request->only(['title','status','category_id']);
        $subCategory = SubCategory::create($data);
        
        $this -> setImage($request, $subCategory);

        return redirect()->route('subcategories.index')->with('success','Sub Categories Created');
    }

    public function edit($subCategoryId){
        return view('admin.sub_category.edit',[
            'categories' => Category::orderBy('title','ASC')->get(),
            'subCategory' => SubCategory::findOrFail($subCategoryId)
        ]);
    }

    public function update($subCategoryId, SubCategoryRequest $request){
        $subCategory = SubCategory::findOrFail($subCategoryId);

        $this -> setImage($request, $subCategory);

        $updateSub = $request->validated();
        $subCategory->update($updateSub);

        return redirect()->route('subcategories.index')->with('success','Sub Category Updated');
    }

    public function destroy($subCategoryId){
        $subCategory = SubCategory::findOrFail($subCategoryId);
        $subCategory->delete();

        return redirect()->route('subcategories.index')->with('success','Sub Category Deleted');
    }

    public function setImage($request, $subCategory) {
        if (!empty($request->image)) {
                $media = $subCategory -> getFirstMedia('subCategories');
            if ($media) {
                $media -> delete();
            }
            $subCategory -> addMediaFromRequest('image')->toMediaCollection('subCategories');
        }
    }
}