<?php

namespace App\Services;

use App\Enums\Role;
use App\Http\Requests\EmailLoginRequest;
use App\Http\Requests\PhoneLoginRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\VendorRegisterRequest;
use App\Http\Resources\Resource;
use App\Models\User;
use App\Notifications\RegisterNotification;
use App\traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    use ResponseTrait;

    public function userRegister(UserService $userService, UserRequest $request) {
        $user = $userService -> createUser($request);
        $user->notify(new RegisterNotification());

        $data =  new Resource($user);
        return $this -> dataResponse('Register Success', $data);
    }

    public function vendorRegister(UserService $userService, VendorRegisterRequest $request, VendorService $vendorService) {
        $user = $userService -> createUser($request,Role::Vendor);
        $vendor = $vendorService -> createVendor([
            'description' => $request -> description,
            'user_id' => $user->id,
        ])->with('user')->get();
        
        $data =  new Resource($vendor);
        return $this -> dataResponse('Register Success', $data);
    }

    public function loginWithEmail(EmailLoginRequest $request) {
        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) {
            return $this->errorResponse('Incorrect Email Or Password', 401);
        }

        if($this->isChangePaswword($request)){
            return $this->errorResponse('please change password', 200);
        }

        $token = $request->user()->createToken('auth_token')->plainTextToken;
        $data = ['token' => $token];
        return $this->dataResponse('Login Successfully', $data);
    }

    public function loginWithPhone(PhoneLoginRequest $request) {
        $credentials = $request->only('phone', 'password');
  
        if (!auth()->attempt($credentials)) {
            return $this->errorResponse('Incorrect Email Or Password', 401);
        }

        if($this->isChangePaswword($request)){
            return $this->errorResponse('please change password', 200);
        }

        $token = $request->user->createToken('auth_token')->plainTextToken;
        $data = ['token' => $token];
        return $this->dataResponse('Login Successfully', $data);
    }

    public function resetPassword(ResetPasswordRequest $request) {
        $user = User::where('email', $request->email)->first();
        
        $user->update([
            'password' => Hash::make($request->newPassword),
        ]);

        return $this->successResponse('Password Updated');
    }

    public function logout($request){
        $user = $request->user();

        $user->tokens()->delete();

        return $this->successResponse('Successfully logged out', 200);
    }

    public function isChangePaswword($request) {
        $user = $request->user();
        if ($user->role == Role::Agent->value || $user->role == Role::Employee->value) {
            if ($user->new_password == 1) {
                return true;
            }
            return false;
        }
    }    
}