<?php

namespace App\Services;

use App\Enums\Role;
use App\Http\Requests\EmployeeRequest;
use App\Mail\SendMail;
use App\Models\User;
use App\traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class EmployeeService
{
    use HelperTrait;

    public function index(Request $request){
        $users = User::where('role', Role::Employee);
        if (!empty($request->get('keyword'))) {
            $keyword = '%' . $request->get('keyword') . '%';
            $users = $users->where('name', 'like', $keyword);
        }

        return view('admin.employees.list', [
            'employees' => $users->paginate(10),
        ]);
    }

    public function create(){
        return view('admin.employees.create');
    }

    public function store(EmployeeRequest $request){
        $password = $this -> CreateRandomCode();

        $data = $request->validated();
        $employee = User::create($data + [
            'password' => Hash::make($password),
            'new_password' => 1,
            'role' => Role::Employee,
        ]);

        $employee->assignRole($employee->role);

        Mail::to($request->email)->send(new SendMail($password,'Login With This Password'));

        return redirect()->route('employees.index')->with('success', 'Employee Created');
    }

    public function edit($id){
        $employee = User::findOrFail($id);

        return view('admin.employees.edit', [
            'employee' => $employee
        ]);
    }

    public function update($id, EmployeeRequest $request){
        $employee = User::findOrFail($id);

        $data = $request->validated();
        $employee->update($data);

        return redirect()->route('employees.index')->with('success', 'Employee Updated');
    }

    public function destroy($id){
        User::destroy($id);
        return redirect()->route('employees.index')->with('success', 'Employee Deleted');
    }
}
