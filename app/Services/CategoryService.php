<?php

namespace App\Services;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Models\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryService
{
    public function index(Request $request){
        $categories = Category::latest();
        
        if (!empty($request->get('keyword'))) {
            $keyword = '%' . $request->get('keyword') . '%';
            $categories = $categories->where('title', 'like', $keyword);
        }
        
        return view('admin.category.list', [
            'categories' => $categories->paginate(10),
        ]);
    }

    public function store(CategoryRequest $request){
        $data = $request->only(['title', 'status']);
        $category = Category::create($data);
        
        $this -> sizes($request, $category);

        $category -> addMediaFromRequest('image') -> toMediaCollection('categories');

        return redirect()->route('categories.index')->with('success','Category Created');
    }

    public function update(CategoryRequest $request, $categoryId){
        $category = Category::find($categoryId);

        $this -> handleImageUpdate($request, $category);

        $data = $request -> only(['title', 'status']);
        $category->update($data);

        $this -> sizes($request, $category);

        return redirect()->route('categories.index')->with('success','Category Updated');
    }

    public function destroy($categoryId){
        $category = Category::findOrFail($categoryId);
        $category->delete();

        return redirect()->route('categories.index')->with('success','Category Deleted');
    }

    public function handleImageUpdate($request, $category) {
        if(!empty($request->image)){
            $file_path = $category->getFirstMedia('categories')->getPath();
            if(!$file_path){
                unlink($file_path);
            }
            $category -> getFirstMedia('categories') -> delete();
            $category -> addMediaFromRequest('image') -> toMediaCollection('categories');
        }
    }

    public function sizes($request, $category){
        if (!empty($request->sizes) && is_array($request->sizes)) {
            Size::where('category_id', $category->id)->delete();
            
            $sizes = [];
            foreach($request->sizes as $size){
                $sizes []= ['category_id'=>$category->id, 'size' => $size];
            }
            DB::table('sizes')->insert($sizes);
        }
    }
}
