<?php

namespace App\Services;

use App\Http\Requests\ReviewRequest;
use App\Http\Resources\Resource;
use App\Models\Product;
use App\Models\Review;
use App\traits\ProductRatingTrait;
use App\traits\ResponseTrait;

class ReviewService
{
    use ResponseTrait, ProductRatingTrait;

    public function store(ReviewRequest $request) {
 
        $product = Product::find($request->productId);

        $reviewData = $request->validated();
        $reviewData['product_id']=$request ->productId;
        $review = Review::create($reviewData + [
            'user_id' => auth()->id()
        ]);

        $this->ProductRating($product);
        
        $data = new Resource($review);
        return $this->dataResponse('Review', $data);
    }

    public function show($id) {
        $review = Review::find($id);
        if (!$review) {
            return $this->errorResponse('No Review For This ID: ' . $id, 404);
        }

        $data = new Resource($review);
        return $this->dataResponse('Review', $data);
    }

    public function update(ReviewRequest $request, $id) {
        // Find Review
        $review = Review::find($id);
        if (!$review) {
            return $this->errorResponse('No Review For This ID: ' . $id, 404);
        }

        // Update Review
        $reviewData = $request->validated();
        $reviewData['product_id']=$request ->productId;

        $review->update($reviewData);

        // Average Rating of product
        $product = Product::find($request -> productId);
        $this->ProductRating($product);

        $data = new Resource($review);
        return $this->dataResponse('Review Updated', $data);
    }

    public function destroy($id) {
        // Find Review
        $review = Review::find($id);
        if (!$review) {
            return $this->errorResponse('No Review For This ID: ' . $id, 404);
        }

        // Average Rating of product
        $product = Product::find($review -> product_id);
        $review->delete();

        $this->ProductRating($product);

        return $this->successResponse('Review Deleted ');
    }
}