<?php

namespace App\Services;

use App\Http\Requests\CouponRequest;
use App\Models\Coupon;
use App\traits\HelperTrait;
use Illuminate\Http\Request;

class CouponService
{
    use HelperTrait;

    public function index(Request $request){
        $coupons = Coupon::latest();
        if (!empty($request->get('keyword'))) {
            $keyword = '%' . $request->get('keyword') . '%';
            $coupons = $coupons->where('code', 'like', $keyword);
        }
        return view('admin.coupons.list', [
            'coupons' => $coupons->paginate(10),
        ]);
    }

    public function create(){
        return view('admin.coupons.create');
    }

    public function store(CouponRequest $request){
        $code = $this->CreateRandomCode();

        $data = $request->validated();
        Coupon::create($data + [
            'code' => $code,
        ]);

        return redirect()->route('coupons.index')->with('success','Category Created');
    }

    public function edit($couponId){
        $coupon = Coupon::findOrFail($couponId);
        return view('admin.coupons.edit',[
            'coupon' => $coupon
        ]);
    }

    public function update($couponId,CouponRequest $request){
        $coupon = Coupon::findOrFail($couponId);

        $data = $request->validated();
        $coupon->update($data);

        return redirect()->route('coupons.index')->with('success','Category Updated');
    }

    public function destroy($couponId){
        Coupon::destroy($couponId);
        return redirect()->route('coupons.index')->with('success','Coupon Deleted');
    }
}
