<?php

namespace App\Services;

use App\Http\Requests\CreateFavoriteRequest;
use App\Http\Resources\Resource;
use App\Models\Favorite;
use App\traits\PermissionCheckTrait;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;

class FavoriteService
{
    use ResponseTrait, PermissionCheckTrait;

    public function index(Request $request){
        $this->PermissionCheck($request, ['favorite view']);
        $user = $request->user();

        $favorites = Favorite::where('user_id', $user->id)->with('product')->paginate(10);
        
        $data = Resource::collection($favorites)->response()->getData(true);
        return $this->dataResponse('All Favorite For Auth User', $data);
    }

    public function store(CreateFavoriteRequest $request){
        Favorite::create([
            'user_id' => auth() -> id(),
            'product_id' => $request -> productId
        ]);
        return $this->successResponse('Favorite Created Successfully');
    }

    public function destroy($request, $favoriteId){
        $this->PermissionCheck($request, ['favorite delete']);
        $favorite = Favorite::destroy($favoriteId);
        if(!$favorite){
            return $this->errorResponse('Favorite Not Found', 404);
        }else{
            return $this->successResponse('Favorite Deleted Successfully');
        };
    }
}
