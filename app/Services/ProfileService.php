<?php

namespace App\Services;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\VerifyEmailRequest;
use App\Http\Resources\Resource;
use App\Models\User;
use App\traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileService
{
    use ResponseTrait;

    public function changePassword(ChangePasswordRequest $request) {
        $user = User::where('email', $request->email)->first();
        if (!Hash::check($request->currentPassword, $user->password)) {
            return $this->errorResponse('Current Password does not match', 403);
        }

        $user->update([
            'password' => Hash::make($request->newPassword),
            'new_password' => 0
        ]);

        return $this->successResponse('Password Changed Successfully');
    }

    public function verifyEmail(VerifyEmailRequest $request) {
        $user = User::where('email', $request->email)->first();

        $user -> email_verified_at = now();
        $user->save();
        return $this -> successResponse('Email Verified');
    }

    public function me($request){
        $data = new Resource($request->user());
        return $this->dataResponse('Auth user', $data);
    }
}