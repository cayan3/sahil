<?php

namespace App\Services;

use App\Enums\Role;
use App\Http\Requests\PaymentRequest;
use App\Http\Resources\Resource;
use App\Models\Agent_Payments;
use App\Models\Group;
use App\Models\User;
use App\traits\ResponseTrait;

class PaymentService
{
    use ResponseTrait;

    public function index(){
        $payments = Agent_Payments::all();

        $data = Resource::collection($payments);
        return $this->dataResponse('Agent Payments', $data);
    }

    public function store(PaymentRequest $request){
        $group = Group::find($request->group_id);

        if ($group->not_recieved_price == 0 || $group->status == 'delivered') {
            return $this->errorResponse('This Group is Paid', 500);
        }

        if($group->agent_id !== $request->agent_id){
            return $this->errorResponse("This group don't belong to this agent", 500);
        }

        $latestPayment = Agent_Payments::where("group_id", $group->id)
                        ->where('agent_id',  $request->agent_id)
                        ->latest()->first();

        $amount_not_recieved = 0;
        if ($latestPayment) {
            
            if ($latestPayment-> amount_not_recieved - $request->amount_recieved < 0) {
                return $this->errorResponse("The remainder in this group is: " . $group-> not_recieved_price, 500);
            }

            $amount_not_recieved = $latestPayment-> amount_not_recieved - $request->amount_recieved;

        }else{
            if ($group-> not_recieved_price - $request->amount_recieved < 0) {
                return $this->errorResponse("The remainder in this group is: " . $group-> not_recieved_price, 500);
            }

            $amount_not_recieved = $group-> not_recieved_price - $request->amount_recieved;
        }

        $agentPaymentData = $request->validated();
        $agentPayment = Agent_Payments::create($agentPaymentData + [
            'amount_not_recieved' =>  $amount_not_recieved,
        ]);

        if ($agentPayment->amount_not_recieved == 0) {
            $group->status = 'delivered';
            $group->save();
        }
        
        $data = new Resource($agentPayment);
        return $this->dataResponse("Payment Created", $data);
    }
    
    public function show(string $id){
        $agentPayment = Agent_Payments::find($id);
        if(!$agentPayment){
            return $this->errorResponse('No Agent Payment for this ID: ' . $id, 404);
        }
        $data = new Resource($agentPayment);
        return $this->dataResponse('Agent Payment', $data);
    }
    
    public function update(PaymentRequest $request, string $id){
        $agentPayment = Agent_Payments::destroy($id);

        if (!$agentPayment) {
            return $this->errorResponse('No payment for this ID: ' . $id, 404);
        }

        $group = Group::find($request->group_id);

        if($group->agent_id !== $request->agent_id){
            return $this->errorResponse("This group don't belong to this agent", 500);
        }

        if($group->status == 'delivered'){
            $group->status = 'processing';
            $group->save();
        }

        $latestPayment = Agent_Payments::where("group_id", $group->id)
                                    ->where('agent_id',  $request->agent_id)->latest()->first();

        $amount_not_recieved = 0;
        if ($latestPayment) {
            
            if ($latestPayment-> amount_not_recieved - $request->amount_recieved < 0) {
                return $this->errorResponse("The remainder in this group is: " . $group-> not_recieved_price, 500);
            }

            $amount_not_recieved = $latestPayment-> amount_not_recieved - $request->amount_recieved;

        }else{
            if ($group-> not_recieved_price - $request->amount_recieved < 0) {
                return $this->errorResponse("The remainder in this group is: " . $group-> not_recieved_price, 500);
            }

            $amount_not_recieved = $group-> not_recieved_price - $request->amount_recieved;
        }

        $agentPayment = Agent_Payments::create([
            'agent_id' =>  $request->agent_id,
            'group_id' =>  $request->group_id,
            'amount_recieved' =>  $request->amount_recieved,
            'amount_not_recieved' =>  $amount_not_recieved,
        ]);

        if ($agentPayment->amount_not_recieved == 0) {
            $group->status = 'delivered';
            $group->save();
        }
       
        return $this->successResponse("Payment Updated");
    }
    
    public function destroy(string $id){
        $agentPayment = Agent_Payments::destroy($id);
        if(!$agentPayment){
            return $this->errorResponse("Not Agent Payment for this ID: " . $id, 404);
        }
        return $this->successResponse("Agent Payment Deleted");
    }

    public function agentPayments(string $id){
        $agent = User::where('id', $id)->where('role', Role::Agent)->first();

        if (!$agent) {
            return $this->errorResponse('No agent for this ID: ' . $id, 404);    
        }

        $payments = Agent_Payments::where('agent_id', $agent->id)->get();

        $data = Resource::collection($payments);
        return $this->dataResponse('Agent Payments', $data);    
    }
}