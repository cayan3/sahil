<?php

namespace App\Enums;

enum Role : String
{
    case Admin = 'admin';
    case User = 'user';
    case Vendor = 'vendor';
    case Employee = 'employee';
    case Agent = 'agent';
}
