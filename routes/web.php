<?php

use App\Http\Controllers\Sahil\api\SocialAuthController;
use App\Http\Controllers\dashboard\admin\AdminLoginController;
use App\Http\Controllers\dashboard\admin\AgentsController;
use App\Http\Controllers\dashboard\admin\BrandController;
use App\Http\Controllers\dashboard\admin\CategoryController;
use App\Http\Controllers\dashboard\admin\CouponController;
use App\Http\Controllers\dashboard\admin\EmployeesController;
use App\Http\Controllers\dashboard\admin\OrderController;
use App\Http\Controllers\dashboard\admin\ProductController;
use App\Http\Controllers\dashboard\admin\SubCategoryController;
use App\Http\Controllers\dashboard\admin\UserController;
use App\Http\Controllers\dashboard\admin\VendorController;
use App\Http\Controllers\dashboard\admin\GroupController;
use App\Http\Controllers\dashboard\admin\PasswordController;
use App\Http\Controllers\dashboard\admin\SettingController;
use App\Http\Controllers\dashboard\vendor\VendorSalesController;
use Illuminate\Support\Facades\Route;

Route::get('forgot-password', [PasswordController::class, 'forgotPassword'])->name('forgot.password');
Route::post('/send-otp-by-email',[PasswordController::class, 'sendOtp'])->name('sendOtp');
Route::get('/enter-otp', [PasswordController::class,'enterOtp'])->name('enter.otp');
Route::get('/enter-password', [PasswordController::class,'enterPassword'])->name('enter.password');
Route::post('/reset-password', [PasswordController::class,'resetPassword'])->name('reset.password');
Route::post('/verify-otp',[PasswordController::class, 'verifyOtp'])->name('verify.otp');

Route::prefix('dashboard')->group(function (){
        Route::get('/login',[AdminLoginController::class,'index'])->name('login');
        Route::post('/authenticate',[AdminLoginController::class,'authenticate'])->name('authenticate');
});

Route::prefix('dashboard')->group(function () {
    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/',[AdminLoginController::class,'dashboard'])->name('dashboard');
        Route::get('/logout',[AdminLoginController::class,'logout'])->name('logout');

        Route::get('/change-password',[PasswordController::class,'changePassword'])->name('change.password');
        Route::post('/update-password',[PasswordController::class,'updatePassword'])->name('update.password');

        Route::resource('categories', CategoryController::class)->except(['show']);
        Route::resource('subcategories',SubCategoryController::class)->except(['show']);
        Route::resource('brands',BrandController::class)->except(['show']);
        Route::resource('coupons',CouponController::class)->except(['show']);

        Route::resource('vendors',VendorController::class)->only(['index','destroy']);
        Route::get('/vendor/sales', [VendorSalesController::class, 'index'])->name('vendor.sales.index');

        Route::resource('orders',OrderController::class)->only(['index','destroy']);
        Route::resource('users',UserController::class)->only(['index']);
        Route::resource('employees',EmployeesController::class)->except(['show']);
        Route::resource('agents',AgentsController::class)->except(['show']);
        Route::resource('groups',GroupController::class)->only(['index']);

        Route::resource('products',ProductController::class)->except(['show','edit','update']);
        Route::get('/products/{product}/details/{productDetail}/edit', [ProductController::class, 'edit'])->name('products.edit');
        Route::put('/products/{productId}/details/{productDetailId}', [ProductController::class, 'update'])->name('products.update');
        
        Route::get('/settings', [SettingController::class, 'index'])->name('settings.index');
        Route::post('/settings/update', [SettingController::class, 'createOrupdate'])->name('settings.update');
    });
    
});

Route::get('/product-subcategories',[ProductController::class,'SubCategoriesToCreateProduct'])->name('product_subcategories.index');
Route::get('/product-sizes',[ProductController::class,'SizesToCreateProduct'])->name('product_sizes.index');



// Social Login Routes
Route::get('auth/google',[SocialAuthController::class, 'redirectToGoogle']); 
Route::get('auth/google/callback',[SocialAuthController::class, 'googleCallback']);
