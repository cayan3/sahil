<?php

use App\Http\Controllers\Sahil\api\AddressController;
use App\Http\Controllers\Sahil\api\AuthController;
use App\Http\Controllers\Sahil\api\BrandController;
use App\Http\Controllers\Sahil\api\CartController;
use App\Http\Controllers\Sahil\api\CategoryController;
use App\Http\Controllers\Sahil\api\CheckoutController;
use App\Http\Controllers\Sahil\api\CityController;
use App\Http\Controllers\Sahil\api\FavoriteController;
use App\Http\Controllers\Sahil\api\GovernorateController;
use App\Http\Controllers\Sahil\api\OrderController;
use App\Http\Controllers\Sahil\api\ProductController;
use App\Http\Controllers\sahil\api\ProfileController;
use App\Http\Controllers\Sahil\api\ReportController;
use App\Http\Controllers\Sahil\api\ReturnController;
use App\Http\Controllers\Sahil\api\ReviewController;
use App\Http\Controllers\Sahil\api\SubCategoryController;
use App\Http\Controllers\Sahil\api\UserController;
use App\Http\Controllers\Sahil_Agents\GroupController;
use App\Http\Controllers\sahil_agents\OrderController as ApiOrderController;
use App\Http\Controllers\sahil_agents\PaymentController;
use App\Http\Controllers\sahil_agents\ProfileController as ApiProfileController;
use App\Http\Controllers\Sahil\api\OtpController;
use App\Http\Controllers\sahil\api\SettingController;
use Illuminate\Support\Facades\Route;

// Otp Routes
Route::post('/send-otp-by-email',[OtpController::class, 'sendOtpByEmail']);
Route::post('/send-otp-by-phone',[OtpController::class, 'sendOtpByPhone']);
Route::post('/verify-otp',[OtpController::class, 'verifyOtp']);

// Checkout Routes
Route::get('/checkout-session',[CheckoutController::class, 'checkout_session']);
Route::get('/checkout-session-success',[CheckoutController::class, 'success'])
    ->name('checkout-session.success');
Route::get('/checkout-session-cancel',[CheckoutController::class, 'cancel'])
    ->name('checkout-session.cancel');
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Vendor Auth Routes
Route::prefix('vendors')->group(function() {
    Route::post('/register',[AuthController::class, 'vendorRegister'])->name('vendors.register');
    Route::post('/email-login',[AuthController::class, 'loginWithEmail'])->name('vendors.email_login');
    Route::post('/phone-login',[AuthController::class, 'loginWithPhone'])->name('vendors.phone_login');
    Route::post('/reset-password',[AuthController::class, 'resetPassword'])->name('vendors.reset-password');
    
    Route::middleware('auth:sanctum')->group(function(){
        Route::get('/me',[ProfileController::class, 'me'])->name('vendors.me');
        Route::post('/logout',[AuthController::class, 'logout'])->name('vendors.logout');
        Route::post('/verify-email',[ProfileController::class, 'verifyEmail']);
        Route::post('/change-password',[ProfileController::class, 'changePassword']);
    });
});
///////////////////////////////////////////////////////////////////////////////////////////////////////

// User Auth Routes
Route::prefix('users')->group(function() {
    Route::post('/register',[AuthController::class, 'userRegister'])->name('users.register');
    Route::post('/email-login',[AuthController::class, 'loginWithEmail'])->name('users.email_login');
    Route::post('/phone-login',[AuthController::class, 'loginWithPhone'])->name('users.phone_login');
    Route::post('/reset-password',[AuthController::class, 'resetPassword'])->name('users.reset-password');
    Route::post('/verify-email',[ProfileController::class, 'verifyEmail']);

    Route::middleware('auth:sanctum')->group(function(){
        Route::get('/me',[ProfileController::class, 'me'])->name('users.me');
        Route::post('/logout',[AuthController::class, 'logout'])->name('users.logout');
        Route::post('/change-password',[ProfileController::class, 'changePassword']);
        Route::post('/change-profile-image',[UserController::class, 'changeProfileImage']);
    });
});
///////////////////////////////////////////////////////////////////////////////////////////////////////

Route::middleware('auth:sanctum')->group(function() {
    // Products Routes
    Route::apiResource('products',ProductController::class);
    Route::get('products/{id}/reports',[ProductController::class, 'getReportsForSpecificProduct']);

    // Addresses Routes
    Route::apiResource('addresses',AddressController::class); 

    // User Routes
    Route::apiResource('users',UserController::class)->except(['index']);

    // Favorites Routes
    Route::apiResource('favorites',FavoriteController::class)->except(['show' , 'update']);

    // Cart Routes
    Route::apiResource('carts',CartController::class)->except(['index']);

    // Order Routes
    Route::apiResource('orders',OrderController::class);
    Route::post('orders/{id}/cancel',[OrderController::class, 'cancel']);
    Route::post('orders/status', [ApiOrderController::class, 'statusOrder']);

    // Return Routes
    Route::apiResource('returns',ReturnController::class);

    // Reports Routes
    Route::apiResource('reports',ReportController::class);

    // Categories Routes
    Route::apiResource('categories',CategoryController::class)->except(['update','destroy','store']);

    // Sub Categories Routes
    Route::apiResource('sub_categories',SubCategoryController::class)->except(['update','destroy','store']);

    // Brands Routes
    Route::apiResource('brands',BrandController::class)->except(['update','destroy','store']);

    // Reviews Routes
    Route::apiResource('reviews',ReviewController::class)->except(['index']);

    // Groups Routes
    Route::apiResource('groups',GroupController::class);
    Route::patch('groups/{id}/update-status',[GroupController::class, 'updateGroupStatus']);

    // Agent Payments Routes
    Route::apiResource('payments',PaymentController::class);

    // Settings Routes
    Route::apiResource('settings', SettingController::class)->only(['index']);
});
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Cities Routes
Route::apiResource('cities', CityController::class)->only(['index']);
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Governorates Routes
Route::apiResource('governorates', GovernorateController::class)->only(['index']);
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Employee Auth Routes
Route::prefix('employees')->group(function() {
    Route::post('/email-login',[AuthController::class, 'loginWithEmail'])->name('employees.email_login');
    Route::post('/phone-login',[AuthController::class, 'loginWithPhone'])->name('employees.phone_login');
    Route::post('/reset-password',[AuthController::class, 'resetPassword'])->name('employees.reset-password');
    Route::post('/change-password',[ApiProfileController::class, 'changePassword']);
    Route::post('/verify-email',[ApiProfileController::class, 'verifyEmail']);
    
    Route::middleware('auth:sanctum')->group(function(){
        Route::get('/me',[ApiProfileController::class, 'me'])->name('employees.me');
        Route::post('/logout',[AuthController::class, 'logout'])->name('employees.logout');
    });
});
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Agents Routes
Route::prefix('agents')->group(function() {
    Route::post('/email-login',[AuthController::class, 'loginWithEmail'])->name('agents.email_login');
    Route::post('/phone-login',[AuthController::class, 'loginWithPhone'])->name('agents.phone_login');
    Route::post('/reset-password',[AuthController::class, 'resetPassword'])->name('agents.reset-password');
    Route::post('/change-password',[ApiProfileController::class, 'changePassword']);
    Route::post('/verify-email',[ApiProfileController::class, 'verifyEmail']);
    
    Route::middleware('auth:sanctum')->group(function(){
        Route::get('/me',[ApiProfileController::class, 'me'])->name('agents.me');
        Route::post('/logout',[AuthController::class, 'logout'])->name('agents.logout');
        
        Route::get('commission', [GroupController::class, 'commission']);
        Route::get('total-amount', [GroupController::class, 'totalAmount']);
        Route::get('{id}/payments', [PaymentController::class, 'agentPayments']);

        // Orders Route in sahil agents
        Route::get('current-orders', [ApiOrderController::class, 'getCurrentOrders']);
        Route::get('previous-orders', [ApiOrderController::class, 'getPreviousOrders']);
        Route::get('canceled-orders', [ApiOrderController::class, 'getCanceledOrders']);
    });
});
///////////////////////////////////////////////////////////////////////////////////////////////////////
