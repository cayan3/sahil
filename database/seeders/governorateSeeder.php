<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class governorateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $governorates_cities = [
            "Alexandria" => ["Alexandria", "Borg El Arab", "Sidi Bishr", "Al Agamy", "Al Max"],
            "Aswan" => ["Aswan", "Kom Ombo", "Edfu", "Nasr City", "Daraw"],
            "Asyut" => ["Asyut", "Dairut", "Manfalut", "Abnub", "El Ghanayem"],
            "Beheira" => ["Damanhur", "Kafr El Dawwar", "Rashid", "Itay El Barud", "Shubrakhit"],
            "Beni Suef" => ["Beni Suef", "Al Fashn", "El Wasta", "Ehnasia", "Nasser"],
            "Cairo" => ["Cairo", "Helwan", "Nasr City", "Zamalek", "Maadi"],
            "Dakahlia" => ["Mansoura", "Talkha", "Mit Ghamr", "Dikirnis", "Matareya"],
            "Damietta" => ["Damietta", "Ras El Bar", "Faraskur", "Kafr Saad", "Ezbet El Burg"],
            "Faiyum" => ["Faiyum", "Ibshaway", "Tamiya", "Sinnuris", "Yusuf al-Siddiq"],
            "Gharbia" => ["Tanta", "Al Mahalla Al Kubra", "Kafr El Zayat", "Zefta", "Samanoud"],
            "Giza" => ["Giza", "6th of October", "Sheikh Zayed", "Hawamdia", "Al Wahat Al Bahriya"],
            "Ismailia" => ["Ismailia", "Fayed", "Qantara", "Abu Suwir", "Tell El Kebir"],
            "Kafr El Sheikh" => ["Kafr El Sheikh", "Desouk", "Sidi Salem", "Baltim", "Metoubes"],
            "Luxor" => ["Luxor", "Armant", "Esna", "Tiba", "El Boghdadi"],
            "Matruh" => ["Marsa Matruh", "El Alamein", "Sidi Barrani", "Negelah", "Sallum"],
            "Minya" => ["Minya", "Mallawi", "Samalut", "Bani Mazar", "Abu Qurqas"],
            "Monufia" => ["Shibin El Kom", "Sadat City", "Menouf", "Ashmoun", "Quesna"],
            "New Valley" => ["Kharga", "Dakhla", "Farafra", "Balat", "Mut"],
            "North Sinai" => ["Arish", "Sheikh Zuweid", "Rafah", "Bir al-Abed", "Hasna"],
            "Port Said" => ["Port Said", "Port Fuad", "El Manzala", "Ard El-Lewa", "Ganoub"],
            "Qalyubia" => ["Banha", "Qalyub", "Shibin El Qanater", "Shubra El Kheima", "Tukh"],
            "Qena" => ["Qena", "Nag Hammadi", "Qift", "Qus", "Dishna"],
            "Red Sea" => ["Hurghada", "Ras Ghareb", "Safaga", "El Quseir", "Marsa Alam"],
            "Sharqia" => ["Zagazig", "Bilbeis", "Minya al-Qamh", "10th of Ramadan City", "Al Ibrahimiya"],
            "Sohag" => ["Sohag", "Akhmim", "Tahta", "Girga", "El Balyana"],
            "South Sinai" => ["Sharm El Sheikh", "Dahab", "Nuweiba", "Taba", "Tor Sinai"],
            "Suez" => ["Suez", "Ain Sokhna", "Al Ganayen", "Ataka", "Faisal"]
        ];

        foreach ($governorates_cities as $governorate => $cities) {
            $governorate_id = DB::table('governorates')->insertGetId(['name' => $governorate]);

            foreach ($cities as $city) {
                DB::table('cities')->insert([
                    'governorate_id' => $governorate_id,
                    'name' => $city
                ]);
            }
        }
    }
}
