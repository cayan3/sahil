<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColorSeeder extends Seeder
{
   
    public function run(): void
    {
        $colorNames = ['red', 'yellow', 'blue', 'green', 'black', 'silver',
        'brown', 'purple', 'pink', 'orange', 'white'];

        $colors = [];
        foreach ($colorNames as $name) {
            $colors[] = ['color' => $name];
        }

        DB::table('colors')->insert($colors);
    }
}
