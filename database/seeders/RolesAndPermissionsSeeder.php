<?php

namespace Database\Seeders;

use App\Enums\Role as EnumsRole;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        
        $permissionsNames = [
            'user create', 'user update', 'user show', 'user delete',
            'address create','address update','address delete','address view',
            'brand view','brand create','brand update','brand delete',
            'cart add','cart update','cart delete','cart view',
            'category view','category create','category update','category delete',
            'sub_category view','sub_category create','sub_category update','sub_category delete',
            'coupon view','coupon create','coupon update','coupon delete',
            'favorite view','favorite create','favorite delete',
            'order create','order update','order delete','order view','order cancel','update order status',
            'product create','product update','product delete','product view',
            'report create','report update','report delete','report view',
            'return create',
            'review create','review update','review delete','review view',
            'sales view',
            'group create', 'group update', 'group delete', 'group view',
            'commission view',
            'totalAmount view',
            'payment create', 'payment update', 'payment delete', 'payment view',
            'agent create', 'agent update', 'agent delete', 'agent view',
            'employee create', 'employee update', 'employee delete', 'employee view',
            'color create', 'color update', 'color delete', 'color view',
            'size create', 'size update', 'size delete', 'size view',
            'setting update'
        ];

        $permission = collect($permissionsNames)->map(function($permission){
            return ['name' => $permission, 'guard_name' => 'web'];
        });

        Permission::insert($permission->toArray());

        $adminRole = Role::create(['name' => EnumsRole::Admin]);
        $adminRole->givePermissionTo(Permission::all());

        $vendorRole = Role::create(['name' => EnumsRole::Vendor]);
        $vendorRole->givePermissionTo(['address create','address update','address delete','address view',
            'cart add','cart update','cart delete','cart view','favorite view','favorite create',
            'favorite delete','order create','order update','order delete','order view',
            'product create','product update','product delete','product view','report create',
            'report update','report delete','report view','return create','review create',
            'review update','review delete','review view','sales view'
        ]);

        $userRole = Role::create(['name' => EnumsRole::User]);
        $userRole->givePermissionTo(['address create','address update','address delete','address view',
            'cart add','cart update','cart delete','cart view','favorite view','favorite create',
            'favorite delete','order create','order update','order delete','order view','order cancel',
            'report create','report update','report delete','report view','return create',
            'review create','review update','review delete','review view'
        ]);

        $employeeRole = Role::create(['name' => EnumsRole::Employee]);
        $employeeRole->givePermissionTo(['group create', 'group update', 'group delete',
            'group view','payment create', 'payment update', 'payment delete', 'payment view',
            'employee update', 'employee delete', 'employee view','update order status'
        ]);

        $agentRole = Role::create(['name' => EnumsRole::Agent]);
        $agentRole->givePermissionTo(['commission view', 'group view', 'totalAmount view',
            'update order status','order view', 'payment view'
        ]);
    }
}
