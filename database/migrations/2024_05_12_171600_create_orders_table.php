<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id('id');
            $table->string('code')->unique();
            $table->enum('order_status',['pending','processing','completed','canceled'])->default('pending');
            $table->float('total_price');
            $table->string('stripe_charge_id')->nullable();
            $table->enum('payment_method',['cash','card']);
            $table->float('shipping_fees')->default(0);
            $table->date('expected_date');
            $table->float('discount_amount')->default(0);
            $table->boolean('is_paid')->default(false);

            $table->foreignId('coupon_id')->nullable()->constrained('coupons')->onDelete('set null');
            $table->foreignId('address_id')->constrained('addresses');
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('group_id')->nullable()->constrained('groups');
                    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
