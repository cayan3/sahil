<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('site_name');
            $table->string('site_email')->unique();
            $table->string('facebook_url')->unique()->nullable();
            $table->string('twitter_url')->unique()->nullable();
            $table->string('instagram_url')->unique()->nullable();
            $table->double('commission');
            $table->enum('commission_status', ['percentage', 'standard']);
            $table->double('shipping_fees');
            $table->enum('shipping_fees_status', ['percentage', 'standard']);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('settings');
    }
};
