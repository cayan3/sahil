@extends('admin.layouts.app')
@section('content')
        <section class="content-header">
            <div class="container-fluid my-2">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Products</h1>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a href="{{route('products.create')}}" class="btn btn-primary">New Product</a>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="container-fluid">
                @include('admin.messages')
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools">
                        <form method="get" action="">
                            <div class="input-group input-group" style="width: 250px;">
                                <input value="{{Request::get('keyword')}}" type="text" name="keyword" class="form-control float-right" placeholder="Search">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </form>
                        <br>
                    {{-- //////////////////////////////////////// --}}
                    <form action="" method="get">
                        <div style="margin: 15px" class="card mb-3">
                            <label style="margin: 10px" for="brand">Brand</label>
                            <select name="brand" id="brand" class="form-control">
                                <option value="">Select Brand</option>
                                @if($brands -> isNotEmpty())
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->id}}">{{$brand->title}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div style="margin: 15px" class="card mb-3">
                            <label style="margin: 10px" for="category">Category</label>
                            <select name="category" id="category" class="form-control @error('category') is-invalid @enderror">
                                <option value="">Select Category</option>
                                @if($categories -> isNotEmpty())
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div style="margin: 15px" class="card mb-3">
                            <label style="margin: 10px" for="category">Sub category</label>
                            <select name="sub_category" id="sub_category" class="form-control @error('sub_category') is-invalid @enderror">
                                <option value="">Select a Sub Category</option>
                            </select>
                        </div>
                        <div style="margin-left: 20px" class="pb-5 pt-3">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </form>        
                        {{-- //////////////////////////////////////// --}}
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                            <tr>
                                <th width="60">ID</th>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Sold</th>
                                <th>Average Rating</th>
                                <th width="100">Status</th>
                                <th width="100">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($products->isNotEmpty())
                                @foreach($products as $product)
                                    @foreach ($product->productDetails as $productDetail)
                                        <tr>
                                            <td>{{$productDetail->id}}</td>
                                            <td>{{$product->title}}</td>
                                            <td>${{$productDetail->price}}</td>
                                            <td>{{$product->quantity}} left in Stock</td>
                                            <td>{{$product->sold}}</td>
                                            <td>{{$product->rating_average}}</td>
                                            @if($product->status == 1)
                                                <td>
                                                    <svg class="text-success-500 h-6 w-6 text-success" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" aria-hidden="true">
                                                        <path stroke-linecap="round" stroke-linejoin="round" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                                    </svg>
                                                </td>
                                            @else
                                                <td>
                                                <svg class="text-danger h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" aria-hidden="true">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                                </svg>
                                                </td>
                                            @endif
                                            <td>
                                                <a href="{{route('products.edit',['product'=>$product->id,'productDetail'=>$productDetail->id])}}">
                                                    <svg class="filament-link-icon w-4 h-4 mr-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                        <path d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z"></path>
                                                    </svg>
                                                </a>
                                            <form id="delete-form" action="{{ route('products.destroy', $product->id) }}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <a onclick="event.preventDefault(); document.getElementById('delete-form').submit();" class="text-danger w-4 h-4 mr-1">                                                <svg wire:loading.remove.delay="" wire:target="" class="filament-link-icon w-4 h-4 mr-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                        <path	ath fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd"></path>
                                                    </svg>
                                                </a>
                                            </form>
                                            </td>
                                        </tr>    
                                    @endforeach
                                @endforeach
                            @else
                            <tr>
                                <td colspan="5">
                                    Records Not Found
                                </td>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        {{$products->links()}}
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </section>
        <!-- /.content -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $('#category').change(function(){
            var categoryId = $(this).val();
            $.ajax({
                url: '{{route('product_subcategories.index')}}',
                type: 'get',
                datatype:'json',
                data: {
                    categoryId: categoryId
                },
                success: function(response){
                    // Success callback function
                   $('#sub_category').find('option').not(':first').remove();
                   $.each(response['subCategories'],function (key,item){
                       $('#sub_category').append(`<option value="${item.id}">${item.title}</option>`)
                   })
                },
                error: function(error){
                    // Error callback function
                    console.log('Something Went Wrong', error)
                }
            });
        });
    </script>
@endsection
