@extends('admin.layouts.app')

@section('content')
{{-- {{dd($errors->all())}} --}}
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid my-2">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Create Product</h1>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="{{route('products.index')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="container-fluid">
            <form action="{{route('products.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <label for="title">Title</label>
                                            <input value="{{ old('title') }}" type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror" placeholder="Title">
                                            @error('title')
                                            <p class="invalid-feedback">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <label for="description">Description</label>
                                            <textarea name="description" id="description" cols="30" rows="10" class="summernote" placeholder="Description"></textarea>
                                            @error('description')
                                            <p class="invalid-feedback">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <h2 class="h4 mb-3">Media</h2>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="formFile" class="form-label">Image</label>
                                        <input class="form-control @error('imageCover')
                                            is-invalid
                                            @enderror" type="file" name="imageCover" id="formFile">
                                        @error('imageCover')
                                        <p class="invalid-feedback">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <h2 class="h4 mb-3">Pricing</h2>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <label for="price">Price</label>
                                            <input value="{{ old('price') }}" type="number" name="price" id="price" class="form-control @error('price') is-invalid @enderror" placeholder="Price">
                                            @error('price')
                                            <p class="invalid-feedback">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <h2 class="h4 mb-3">Color</h2>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <label for="colorSelect" class="form-label">Choose a colors:</label>
                                            <select id="colorSelect" class="form-control @error('color_id') is-invalid @enderror" name="color_id">
                                                <option value="">Select Color</option>
                                                @if($colors -> isNotEmpty())
                                                    @foreach($colors as $color)
                                                        <option value="{{$color->id}}">{{$color->color}}</option>
                                                    @endforeach
                                                @endif
                                            </select>                                               
                                            @error('color_id')
                                            <p class="invalid-feedback">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <h2 class="h4 mb-3">Size</h2>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <label for="sizeSelect" class="form-label">Choose a size:</label>
                                            <select id="sizeSelect" class="form-control @error('size_id') is-invalid @enderror" name="size_id">
                                                <option value="">Select Size</option>
                                            </select>                                               
                                            @error('size_id')
                                            <p class="invalid-feedback">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <label for="quantity">Quantity</label>
                                            <input value="{{ old('quantity') }}" type="number" min="0" name="quantity" id="quantity" class="form-control @error('color') is-invalid @enderror" placeholder="quantity">
                                            @error('quantity')
                                            <p class="invalid-feedback">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card mb-3">
                            <div class="card-body">
                                <h2 class="h4 mb-3">Product status</h2>
                                <div class="mb-3">
                                    <select name="status" id="status" class="form-control @error('status') is-invalid @enderror">
                                        <option value="1">Active</option>
                                        <option value="0">Block</option>
                                    </select>
                                    @error('status')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <h2 class="h4  mb-3">Product category</h2>
                                <div class="mb-3">
                                    <label for="category">Category</label>
                                    <select name="category_id" id="category" class="form-control
                                        @error('category_id') is-invalid @enderror">
                                        
                                        <option value="">Select Category</option>
                                        @if($categories -> isNotEmpty())
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->title}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('category_id')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="sub_category">Sub category</label>
                                    <select name="sub_category_id" id="sub_category" class="form-control @error('sub_category_id') is-invalid @enderror">
                                        <option value="">Select a Sub Category</option>
                                    </select>
                                    @error('sub_category_id')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="card-body">
                                <h2 class="h4 mb-3">Product brand</h2>
                                <div class="mb-3">
                                    <select name="brand_id" id="brand" class="form-control @error('brand_id') is-invalid @enderror">
                                        <option value="">Select Brand</option>
                                        @if($brands -> isNotEmpty())
                                            @foreach($brands as $brand)
                                                <option value="{{$brand->id}}">{{$brand->title}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('brand_id')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pb-5 pt-3">
                    <button class="btn btn-primary">Create</button>
                    <a href="{{route('products.index')}}" class="btn btn-outline-dark ml-3">Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $('#category').change(function(){
            var categoryId = $(this).val();
            $.ajax({
                url: '{{route('product_subcategories.index')}}',
                type: 'get',
                datatype:'json',
                data: {
                    categoryId: categoryId
                },
                success: function(response){
                    $('#sub_category').find('option').not(':first').remove();
                    $.each(response['subCategories'],function (key,item){
                        $('#sub_category').append(`<option value="${item.id}">${item.title}</option>`)
                    })
                },
                error: function(error){
                    console.log('Something Went Wrong', error)
                }
            });
        });
    </script>
    <script>
        $('#category').change(function(){
            var categoryId = $(this).val();
            $.ajax({
                url: '{{route('product_sizes.index')}}',
                type: 'get',
                datatype:'json',
                data: {
                    categoryId: categoryId
                },
                success: function(response){
                    $('#sizeSelect').find('option').not(':first').remove();
                    $.each(response['sizes'],function (key,item){
                        $('#sizeSelect').append(`<option value="${item.id}">${item.size}</option>`)
                    })
                },
                error: function(error){
                    console.log('Something Went Wrong', error)
                }
            });
        });
    </script>
@endsection
