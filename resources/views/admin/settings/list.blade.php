@extends('admin.layouts.app')

@section('content')

@include('admin.messages')
    <br>
    <!-- Main content -->
    <section class="content">
        {{--@if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif --}}

            <!-- Default box -->
        <div class="container-fluid">
            <form action="{{route('settings.update')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            {{-- Site name --}}
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="site_name">Site Name</label>
                                    <input type="text" value="{{$setting ? $setting->site_name : ''}}" name="site_name" id="site_name" class="form-control @error('site_name')
                                        is-invalid
                                        @enderror" placeholder="Site Name">
                                    @error('site_name')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            {{-- Site email --}}
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="site_email">Site Email</label>
                                    <input type="text" value="{{$setting ? $setting->site_email : ''}}" name="site_email" id="site_email" class="form-control @error('site_email')
                                        is-invalid
                                        @enderror" placeholder="Site Email">
                                    @error('site_email')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            {{-- Site facebook --}}
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="facebook_url">Facebook URL</label>
                                    <input type="text" value="{{$setting ? $setting->facebook_url : ''}}" name="facebook_url" id="facebook_url" class="form-control @error('facebook_url')
                                        is-invalid
                                        @enderror" placeholder="Facebook URL">
                                    @error('facebook_url')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            {{-- Site twitter --}}
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="twitter_url">Twitter URL</label>
                                    <input type="text" value="{{$setting ? $setting->twitter_url : ''}}" name="twitter_url" id="twitter_url" class="form-control @error('twitter_url')
                                        is-invalid
                                        @enderror" placeholder="Twitter URL">
                                    @error('twitter_url')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            {{-- Site instagram --}}
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="instagram_url">Instagram URL</label>
                                    <input type="text" value="{{$setting ? $setting->instagram_url : ''}}" name="instagram_url" id="instagram_url" class="form-control @error('instagram_url')
                                        is-invalid
                                        @enderror" placeholder="Instagram URL">
                                    @error('instagram_url')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            {{-- Site Logo --}}
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="site_logo" class="form-label">Site Logo</label>
                                    <input class="form-control @error('site_logo')
                                        is-invalid
                                        @enderror" type="file" name="site_logo" id="formFile">
                                    @error('site_logo')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            {{-- Site commission --}}
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="commission">Commission</label>
                                    <input type="text" value="{{$setting ? $setting->commission : ''}}" name="commission" id="commission" class="form-control @error('commission')
                                        is-invalid
                                        @enderror" placeholder="Commission">
                                    @error('commission')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            {{-- Site commission status--}}
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="commission_status">Commission Status</label>
                                    <select name="commission_status" id="commission_status" class="form-control @error('commission_status')
                                            is-invalid
                                            @enderror" placeholder="Commission Status">
                                        <option value='percentage' {{$setting ? $setting->commission_status == 'percentage' ? 'selected' : '' : ''}}>percentage</option>
                                        <option value='standard' {{$setting ? $setting->commission_status == 'standard' ? 'selected' : '' : ''}}>standard</option>
                                    </select>
                                    @error('commission_status')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            {{-- Site shipping fees --}}
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="shipping_fees">Shipping Fees</label>
                                    <input type="text" value="{{$setting ? $setting->shipping_fees : ''}}" name="shipping_fees" id="shipping_fees" class="form-control @error('shipping_fees')
                                        is-invalid
                                        @enderror" placeholder="Shipping Fees">
                                    @error('shipping_fees')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            {{-- Site shipping fees status--}}
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="shipping_fees_status">Shipping Fees Status</label>
                                    <select name="shipping_fees_status" id="shipping_fees_status" class="form-control @error('shipping_fees_status')
                                            is-invalid
                                            @enderror" placeholder="Shipping Fees Status">
                                        <option value='percentage' {{$setting ? $setting->shipping_fees_status == 'percentage' ? 'selected' : '' : ''}}>percentage</option>
                                        <option value='standard' {{$setting ? $setting->shipping_fees_status == 'standard' ? 'selected' : '' : ''}}>standard</option>
                                    </select>
                                    @error('shipping_fees_status')
                                    <p class="invalid-feedback">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pb-5 pt-3">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
@endsection