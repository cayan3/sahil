@extends('admin.layouts.app')
@section('content')
        <section class="content-header">
            <div class="container-fluid my-2">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Sales</h1>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="container-fluid">
                @include('admin.messages')
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools">
                        <form method="get" action="">
                            <div class="input-group input-group" style="width: 250px;">
                                <input value="{{Request::get('keyword')}}" type="text" name="keyword" class="form-control float-right" placeholder="Search">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </form>
                        <br>
                    {{-- //////////////////////////////////////// --}}
                    <form action="" method="get">
                        <div style="margin: 15px" class="card mb-3">
                            <label style="margin: 10px" for="brand">Brand</label>
                            <select name="brand" id="brand" class="form-control">
                                <option value="">Select Brand</option>
                                @if($brands -> isNotEmpty())
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->id}}">{{$brand->title}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div style="margin: 15px" class="card mb-3">
                            <label style="margin: 10px" for="category">Category</label>
                            <select name="category" id="category" class="form-control @error('category') is-invalid @enderror">
                                <option value="">Select Category</option>
                                @if($categories -> isNotEmpty())
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div style="margin: 15px" class="card mb-3">
                            <label style="margin: 10px" for="category">Sub category</label>
                            <select name="sub_category" id="sub_category" class="form-control @error('sub_category') is-invalid @enderror">
                                <option value="">Select a Sub Category</option>
                            </select>
                        </div>
                        <div style="margin-left: 20px" class="pb-5 pt-3">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </form>        
                        {{-- //////////////////////////////////////// --}}
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Sold</th>
                                <th>Product Sales</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($products->isNotEmpty())
                                @foreach($products as $product)
                                    <tr>
                                        <td><a href="#">{{$product->title}}</a></td>
                                        <td>${{$product->price}}</td>
                                        <td>{{$product->quantity}} left in Stock</td>
                                        <td>{{$product->sold}}</td>
                                        <th>{{$product->sold * $product->price}}</th>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td>Total Sales</td>
                                    <td>${{$totalSales}}</td>
                                </tr>
                            @else
                            <tr>
                                <td colspan="5">
                                    Records Not Found
                                </td>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        {{$products->links()}}
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </section>
        <!-- /.content -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
            $('#category').change(function(){
                var categoryId = $(this).val();
                $.ajax({
                    url: '{{route('product_subcategories.index')}}',
                    type: 'get',
                    datatype:'json',
                    data: {
                        categoryId: categoryId
                    },
                    success: function(response){
                        // Success callback function
                       $('#sub_category').find('option').not(':first').remove();
                       $.each(response['subCategories'],function (key,item){
                           $('#sub_category').append(`<option value="${item.id}">${item.title}</option>`)
                       })
                    },
                    error: function(error){
                        // Error callback function
                        console.log('Something Went Wrong', error)
                    }
                });
                });
    </script>
@endsection
